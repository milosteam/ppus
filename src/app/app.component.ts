import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  Router,
  NavigationEnd,
  ActivatedRoute,
  NavigationStart,
  RouterOutlet
} from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { merge, Observable, of } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Logger, I18nService, untilDestroyed } from 'src/app/core';
import * as AOS from 'aos';
import { RequestService } from './core/http/request.service';
import { SeoService } from './core/seo.service';
import { fader } from './route-animations';
import { LogoService } from './shell/services/logo.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

const log = new Logger('App');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fader]
})
export class AppComponent implements OnInit, OnDestroy {
  pageIsLoading: Observable<boolean>;
  spinner = faSpinner;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private logo: LogoService,
    private seoService: SeoService,
    private translateService: TranslateService,
    private i18nService: I18nService,
    private http: RequestService
  ) { }

  ngOnInit() {
     this.logo.setPageIsLoading(false);
    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }
    this.pageIsLoading = this.logo.pageIsLoading$;
    log.debug('init');

    this.http.loginClient({ 'appname': environment.appName }).subscribe();

    // Setup translations
    this.i18nService.init(
      environment.defaultLanguage,
      environment.supportedLanguages
    );



    const onNavigationEnd = this.router.events.pipe(
      tap(event => event instanceof NavigationStart ? this.logo.setPageIsLoading(true) : false),
      filter(event => event instanceof NavigationEnd)
    );

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        switchMap(route => route.data),
        untilDestroyed(this)
      )
      .subscribe(event => {
        this.logo.setPageIsLoading(true);
        this.prepareRoute();
        this.i18nService.setSeoData();
      });

    AOS.init();
  }

  prepareRoute() {
    return 'fader'
  }

  ngOnDestroy() {
    this.i18nService.destroy();
  }
}
