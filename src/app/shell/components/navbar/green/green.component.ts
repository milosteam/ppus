import { Component, HostListener, OnInit } from '@angular/core';
import { faRecycle, faSeedling, faSolarPanel } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'pps-green',
  templateUrl: './green.component.html',
  styleUrls: ['./green.component.scss']
})
export class GreenComponent implements OnInit {
  faSeedling = faSeedling;
  faRecycle = faRecycle;
  faSolar = faSolarPanel;
  private greenStatus = false;
  constructor() { }

  ngOnInit(): void {
  }

  private isTouch(): number | true {
    return 'ontouchstart' in window || navigator.maxTouchPoints;
  }

  onMobileOpen(object: any): void {
   console.log(object);

  }

  onGreenHover(event: PointerEvent): void {
    if (this.isTouch()){
      console.log('Tu sam');
      event.preventDefault();
      event.stopPropagation();
    }
  }
}
