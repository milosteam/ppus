import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { faSignInAlt, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-navbar-default',
  templateUrl: './navbar-default.component.html',
  styleUrls: ['./navbar-default.component.scss']
})
export class NavbarDefaultComponent implements OnInit {
  @Input()
  useOnlyDarkLogo: boolean;

  @Input()
  darkLinks: boolean;

  @Input()
  position: string;

  signInAlt = faSignInAlt;
  userPlus = faUserPlus;

  path: Observable<string>;

  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.path = this.cnts.$lngPath;
  }

  isRightPositioned() {
    return this.position === 'right';
  }
}
