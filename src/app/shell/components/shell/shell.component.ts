import { AfterContentInit, Component, OnInit } from '@angular/core';
import { faLeaf } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CookieconsentService } from '../../papirprint/services/cookieconsent.service';
import { LogoService } from '../../services/logo.service';
import {fader} from '../../../route-animations';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  isDark$: Observable<boolean>;
  isConsent: Observable<boolean>;
  constructor(private logos: LogoService, private cks: CookieconsentService) {
    this.isDark$ = this.logos.logo$;
   }

  ngOnInit() {
    this.isConsent = this.cks.$consentCookie;
  }

  prepareRoute (){
    return 'fader'
  }

}
