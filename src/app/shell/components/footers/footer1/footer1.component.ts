import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import {
  faFacebook,
  faTwitter,
  faInstagram
} from '@fortawesome/free-brands-svg-icons';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-footer1',
  templateUrl: './footer1.component.html',
  styleUrls: ['./footer1.component.scss']
})
export class Footer1Component implements OnInit {
  fa = {
    faFacebook: faFacebook,
    faTwitter: faTwitter,
    faInstagram: faInstagram
  };

  path: Observable<string>;

  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.path = this.cnts.$lngPath;
  }
}
