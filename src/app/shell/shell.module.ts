import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';


import { ShellComponent } from './components/shell/shell.component';
import { NavbarDefaultComponent } from './components/navbar/navbar-default/navbar-default.component';
import { NavbarShellComponent } from './components/navbar/navbar-shell/navbar-shell.component';
import { Footer1Component } from './components/footers/footer1/footer1.component';
import { LngddComponent } from './papirprint/components/lngdd/lngdd.component';
import { NavbarsrComponent } from './papirprint/components/navbars/navbarsr/navbarsr.component';
import { NavbarenComponent } from './papirprint/components/navbars/navbaren/navbaren.component';
import { SearchComponent } from './papirprint/components/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Footer1srComponent } from './papirprint/components/navbars/footer1sr/footer1sr.component';
import { Footer1enComponent } from './papirprint/components/navbars/footer1en/footer1en.component';
import { GoogleanalytcsComponent } from './papirprint/components/googleanalytcs/googleanalytcs.component';
import { GreenComponent } from './components/navbar/green/green.component';

@NgModule({
  imports: [
    SharedModule,
    NgbModule,
    TranslateModule.forChild({ extend: true }),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    ShellComponent,
    NavbarDefaultComponent,
    NavbarShellComponent,
    LngddComponent,
    NavbarsrComponent,
    NavbarenComponent,
    SearchComponent,
    Footer1srComponent,
    Footer1enComponent,
    Footer1Component,
    GoogleanalytcsComponent,
    GreenComponent
  ],
  exports: [
    NavbarDefaultComponent,
    LngddComponent,
    SearchComponent,
    Footer1srComponent,
    Footer1enComponent,
    TranslateModule
  ]
})
export class ShellModule {}
