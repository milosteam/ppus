import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { isPlatformBrowser } from '@angular/common';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CookieconsentService {
  public $consentCookie: Observable<boolean>;
  private $consentCookieSub = new BehaviorSubject(false as boolean);
  private ga = environment.googleAnalyticsTrackingCode;

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject(DOCUMENT) private document: Document
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.init();
      this.$consentCookie = this.$consentCookieSub
        .asObservable()
        .pipe(shareReplay());
    }
  }

  public init() {
    const cookies = this.document.cookie.split(';');
    let c = '';
    cookies.map(cookie => {
      c = cookie.replace(/^\s+/g, '');
      if (c.includes('papirpint_consent') && c.indexOf('papirpint_consent') === 0) {
        if (c.substring('papirpint_consent'.length + 1, c.length) === 'deny') {
          window['ga-disable-' + this.ga] = true;
          this.eraseCookie('_ga');
          this.eraseCookie('_ga_RHM9R8JZ5X');
          this.$consentCookieSub.next(false);
        }
        else {
          window['ga-disable-' + this.ga] = false
          this.$consentCookieSub.next(true);
        }
      }
    });
  }

  public setCookiConsent(agree: boolean) {
    if (agree === true || agree === false) {
      this.$consentCookieSub.next(agree);
      if (agree === false) {
         window.location.reload();
      }
      if(agree === true){
        window['ga-disable-' + this.ga] = false;
      }
    }
  }

  private eraseCookie(name) {
    this.document.cookie = name + '=; Path=/; Domain='+ environment.domain +'; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

}
