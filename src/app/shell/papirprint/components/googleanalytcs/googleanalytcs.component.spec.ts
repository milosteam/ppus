import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleanalytcsComponent } from './googleanalytcs.component';

describe('GoogleanalytcsComponent', () => {
  let component: GoogleanalytcsComponent;
  let fixture: ComponentFixture<GoogleanalytcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoogleanalytcsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleanalytcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
