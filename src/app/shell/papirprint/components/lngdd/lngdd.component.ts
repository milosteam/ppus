import { Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { LangddService } from 'src/app/core/langdd.service';
import { DOCUMENT } from '@angular/common';

import { isPlatformBrowser } from '@angular/common';
// Dynamic page content
import { COOKIECONSENTSR } from 'src/app/pptexts/sr/cookieconsent';
import { COOKIECONSENTEN } from 'src/app/pptexts/en/cookieconsent';
import { CookieconsentService } from '../../services/cookieconsent.service';
import { I18nService } from 'src/app/core';
import { ContentService } from 'src/app/core/content.service';
import { Router } from '@angular/router';

@Component({
  selector: 'dc-lngdd',
  templateUrl: './lngdd.component.html',
  styleUrls: ['./lngdd.component.scss']
})
export class LngddComponent implements OnInit, OnDestroy {
  langDescription: string;
  private cookie;
  private cookieContent = COOKIECONSENTSR;

  private _languages = [
    { code: 'sr', description: 'Srpski' },
    { code: 'en', description: 'English' }
  ];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: any,
    private lngs: LangddService,
    private cs: CookieconsentService,
    private cnts: ContentService,
    private i18nService: I18nService
  ) { }

  ngOnInit(): void {
      this.lngs.$language.subscribe(lng => {
      if (lng) {
        this.cnts.setContentData(lng);
        this.langDescription = lng.description;
        switch (lng.code) {
          case 'sr':
            this.cookieContent = COOKIECONSENTSR;
            this.i18nService.language = 'sr-RS';
            this.cookieConsent();
            break;
          case 'en':
            this.cookieContent = COOKIECONSENTEN;
            this.i18nService.language = 'en-US';
            this.cookieConsent();
            break;
          default:
            this.cookieContent = COOKIECONSENTSR;
            this.cookieConsent();
        }
        this.i18nService.getNewRoutePath();
      } else {
        // this.optionClicked();
      }
    });
  }

  optionClicked(lang: any = { code: 'sr', description: 'Srpski' }) {
    if (lang && lang.description !== this.langDescription) {
      this.lngs.setLanguage(lang);
      this.langDescription = lang.description;
    }
  }

  public languages() {
    return this._languages;
  }

  cookieConsent() {
    const cc = window as any;
    const service = this.cs;
    if (this.cookie) {
      this.cookie = null;
      this.document.querySelector('[aria-label="cookieconsent"]').remove();
      this.document.getElementsByClassName('cc-revoke')[0].remove();
    }
    this.cookie = new cc.cookieconsent.Popup({
      cookie: {
        name: 'papirpint_consent'
      },
      theme: 'classic',
      type: 'opt-out',
      revokable: true,
      animateRevokable: false,
      enabled: true,
      palette: {
        popup: {
          background: '#053b79'
        },
        button: {
          background: '#fff',
          text: ' #053b79'
        }
      },
      content: {
        header: this.cookieContent.header,
        message: this.cookieContent.message,
        dismiss: this.cookieContent.dismiss,
        allow: this.cookieContent.allow,
        deny: this.cookieContent.deny,
        link: this.cookieContent.link,
        href: this.cookieContent.href,
        close: '&#x274c;'
        // href: environment.Frontend + "/dataprivacy"
      },
      onStatusChange(status: string, chosenBefore: string) {
        if (status === 'allow') {
          service.setCookiConsent(true);
        }
        if (status === 'deny') {
          service.setCookiConsent(false);
        }
      }
    });
  }

  lngRouteRedirect() { }

  ngOnDestroy(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.cookie = null;
      this.document.querySelector('[aria-label="cookieconsent"]').remove();
      this.document.getElementsByClassName('cc-revoke')[0].remove();
    }
  }
}
