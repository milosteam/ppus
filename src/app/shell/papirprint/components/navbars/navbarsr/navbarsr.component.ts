import { Component, Input, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-navbarsr',
  templateUrl: './navbarsr.component.html',
  styleUrls: ['./navbarsr.component.scss']
})
export class NavbarsrComponent implements OnInit {
  constructor(private cnts: ContentService) {}

  ngOnInit(): void {}
}
