import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NavbarsrComponent } from './navbarsr.component';

describe('NavbarsrComponent', () => {
  let component: NavbarsrComponent;
  let fixture: ComponentFixture<NavbarsrComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [NavbarsrComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarsrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
