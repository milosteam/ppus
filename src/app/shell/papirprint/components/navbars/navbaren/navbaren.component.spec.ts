import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NavbarenComponent } from './navbaren.component';

describe('NavbarenComponent', () => {
  let component: NavbarenComponent;
  let fixture: ComponentFixture<NavbarenComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [NavbarenComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
