import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Footer1enComponent } from './footer1en.component';

describe('Footer1enComponent', () => {
  let component: Footer1enComponent;
  let fixture: ComponentFixture<Footer1enComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [Footer1enComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(Footer1enComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
