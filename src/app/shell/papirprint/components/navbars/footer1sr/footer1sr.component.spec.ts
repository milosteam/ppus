import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Footer1srComponent } from './footer1sr.component';

describe('Footer1srComponent', () => {
  let component: Footer1srComponent;
  let fixture: ComponentFixture<Footer1srComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [Footer1srComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(Footer1srComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
