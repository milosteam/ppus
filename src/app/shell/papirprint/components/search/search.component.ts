import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { I18nService } from 'src/app/core';
import {
  faSearch,
  faSpinner,
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'dc-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  search = faSearch;
  spinner = faSpinner;
  x = faTimes;

  subscription: Subscription;

  searchControl: FormControl = new FormControl('');

  selected = '';

  isLoading = false;

  filteredList: any[] | number = null;

  constructor(private i18n: I18nService) {}

  ngOnInit() {
    this.subscription = this.searchControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(1000),
        map(text => this._filter(text))
      )
      .subscribe(data => {
        if (data && data[0]) {
          this.filteredList = data;
          this.isLoading = false;
        } else {
          this.resetSrch();
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  resetSrch() {
    this.searchControl.setValue('');
    this.filteredList = null;
    this.selected = null;
  }

  onSelectedOption(event: Event, path: string) {
    event.preventDefault;
  }

  private _filter(value: string): any[] | number {
    this.filteredList = [];
    this.isLoading = true;
    const filterValue = value.toLowerCase().trim();
    const query =
      !this.selected || this.selected.toLowerCase().trim() !== filterValue;
    if (query) {
      if (filterValue && filterValue !== '') {
        return this.i18n.searchForPage(filterValue);
      } else {
        this.isLoading = false;
        return null;
      }
    }
  }
}
