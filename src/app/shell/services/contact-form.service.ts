import { Injectable } from '@angular/core';
import { RequestService } from '../../core/http/request.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactFormService {

  constructor(private http: RequestService) { }

  sendContatData(data: any): Observable<any> {
    return this.http.post(environment.APPURL + '/sendcontact', data);
  }

}
