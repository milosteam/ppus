import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogoService {
  pageIsLoading$: Observable<any>;
  logo$: Observable<any>;
  private logoSub$ = new BehaviorSubject(true);
  private pageIsLoadingSub$ = new BehaviorSubject(false);
  constructor() {
    this.logo$ = this.logoSub$.asObservable();
    this.pageIsLoading$ = this.pageIsLoadingSub$.asObservable();
  }

  setLogo(is: boolean) {
    if (is === true || is === false) {
      this.logoSub$.next(is);
    }
  }

  setPageIsLoading(loading: boolean) {
    if (loading === true || loading === false) {
      if (loading === false) {
        timer(1000).subscribe(x => {
          this.pageIsLoadingSub$.next(loading)
        });
      } else {
        this.pageIsLoadingSub$.next(loading);
      }
    }
  }


}
