import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from 'src/app/core/http/request.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobFormService {

  constructor(private http: RequestService) { }

  sendCvData(data: any): Observable<any> {
    return this.http.postFileUpload(environment.APPURL + '/cv', data);
  }
}
