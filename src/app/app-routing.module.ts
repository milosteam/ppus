import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from 'src/app/shell/services/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    {
      // SR
      path: 'sr/pocetna',
      loadChildren: () =>
        import('./pages/pocetna/pocetna.module').then(m => m.PocetnaModule),
      data: {animation: 'spin'}
    },
  
    {
      path: 'sr/proizvodni-program/proizvodni-program',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'sr/proizvodni-program/ambalaza-prehrambena-industrija',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'sr/proizvodni-program/ambalaza-voda-sokovi',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'sr/proizvodni-program/ambalaza-kozmetika-farmacija',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'sr/proizvodni-program/proizvodnja-pe-filma',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    // {
    //   path: 'sr/proizvodni-program/ambalaza-petfood-program',
    //   loadChildren: () =>
    //     import('./pages/online-payment/online-payment.module').then(
    //       m => m.OnlinePaymentModule
    //     )
    // },
    {
      path: 'sr/proizvodni-program/ambalaza-petfood-program/:product',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'sr/proizvodni-program/ambalaza-kozmetika-farmacija/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    {
      path: 'sr/proizvodni-program/ambalaza-voda-sokovi/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    {
      path: 'sr/proizvodni-program/ambalaza-prehrambena-industrija/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },

    {
      path: 'sr/tehnickekarakteristike',
      loadChildren: () =>
        import('./pages/tehkarakteristike/tehkarakteristike.module').then(
          m => m.TehkarakteristikeModule
        )
    },
    {
      path: 'sr/onama',
      loadChildren: () =>
        import('./pages/onama/onama.module').then(m => m.OnamaModule)
    },
    {
      path: 'sr/kontakt',
      loadChildren: () =>
        import('./pages/contact/contact.module').then(m => m.ContactModule)
    },
    {
      path: 'sr/politika_privatnosti',
      loadChildren: () =>
        import('./pages/legal/legal.module').then(m => m.LegalModule)
    },
    {
      path: 'sr/karijera',
      loadChildren: () =>
        import('./pages/posao/posao.module').then(m => m.PosaoModule)
    },

    // END SR

    // EN
    {
      path: 'en/home',
      loadChildren: () =>
        import('./pages/pocetna/pocetna.module').then(m => m.PocetnaModule)
    },
    //
    {
      path: 'en/production-program/packaging-food-industry',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'en/production-program/packaging-food-industry/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    //
    {
      path: 'en/production-program/packaging-beverage',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'en/production-program/packaging-beverage/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    //
    {
      path: 'en/production-program/packaging-pharmaceutical-cosmetic',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'en/production-program/packaging-pharmaceutical-cosmetic/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    //
    {
      path: 'en/production-program/production-pe-films',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    //
    {
      path: 'en/production-program/packaging-petfood-products',
      loadChildren: () =>
        import('./pages/online-payment/online-payment.module').then(
          m => m.OnlinePaymentModule
        )
    },
    {
      path: 'en/production-program/packaging-petfood-products/:product',
      loadChildren: () =>
        import('./pages/products/products.module').then(m => m.ProductsModule)
    },
    //
    {
      path: 'en/technical-characteristics',
      loadChildren: () =>
        import('./pages/tehkarakteristike/tehkarakteristike.module').then(
          m => m.TehkarakteristikeModule
        )
    },
    {
      path: 'en/about-the-company',
      loadChildren: () =>
        import('./pages/onama/onama.module').then(m => m.OnamaModule)
    },
    {
      path: 'en/contact',
      loadChildren: () =>
        import('./pages/contact/contact.module').then(m => m.ContactModule)
    },
    {
      path: 'en/privacy_policy',
      loadChildren: () =>
        import('./pages/legal/legal.module').then(m => m.LegalModule)
    }

    // END EN
  ]),

  // Fallback when no prior route is matched
  { path: '**', data: { routeId: 1 }, redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      relativeLinkResolution: 'legacy',
      initialNavigation: 'enabled'
    })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
