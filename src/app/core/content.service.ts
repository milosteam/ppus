import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PROIZPROGRAMSR } from '../pptexts/sr/proizvodniprogram';
import { PROIZPROGRAMEN } from '../pptexts/en/proizvodniprogram';
import { SERTIFIKATISR } from '../pptexts/sr/sertifikati';
import { SERTIFIKATIEN } from '../pptexts/en/sertifikati';
import { TEHNOLOGIJEPOCETNASR } from '../pptexts/sr/tehnologije';
import { TEHNOLOGIJEPOCETNAEN } from '../pptexts/en/tehnologije';
import { ONAMASR } from '../pptexts/sr/onama';
import { ONAMAEN } from '../pptexts/en/onama';
import { TEHKARAKTERISTIKESR2 } from '../pptexts/sr/tehnickekarakteristike';
import { TEHKARAKTERISTIKEEN2 } from '../pptexts/en/tehnickekarakteristike';
import { LEGALSR } from '../pptexts/sr/legal';
import { LEGALEN } from '../pptexts/en/legal';
import { CONTACTSR } from '../pptexts/sr/contact';
import { CONTACTEN } from '../pptexts/en/contact';
import { POSAOSR } from '../pptexts/sr/posao';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ContentService {
  public $lngPath: Observable<any>;
  public $proizvodniProgram: Observable<any>;
  public $certificates: Observable<any>;
  public $tehnologije: Observable<any>;
  public $tehkarakteristike: Observable<any>;
  public $oNama: Observable<any>;
  public $legal: Observable<any>;
  public $contact: Observable<any>;
  public $posao: Observable<any>;

  private $lngPathSub = new BehaviorSubject('sr');
  private $proizvodniProgramSub = new BehaviorSubject(null);
  private $certificatesSub = new BehaviorSubject(null);
  private $tehnologijeSub = new BehaviorSubject(null);
  public $oNamaSub = new BehaviorSubject(null);
  public $tehKarakteristikeSub = new BehaviorSubject(null);
  public $legalSub = new BehaviorSubject(null);
  public $contactSub = new BehaviorSubject(null);
  public $posaoSub = new BehaviorSubject(null);

  constructor(private router: Router) {
    this.$lngPathSub.next('sr');
    this.$proizvodniProgramSub.next(PROIZPROGRAMSR);
    this.$certificatesSub.next(SERTIFIKATISR);
    this.$tehnologijeSub.next(TEHNOLOGIJEPOCETNASR);
    this.$oNamaSub.next(ONAMASR);
    this.$tehKarakteristikeSub.next(TEHKARAKTERISTIKESR2);
    this.$legalSub.next(LEGALSR);
    this.$contactSub.next(CONTACTSR);
    this.$posaoSub.next(POSAOSR);

    this.$lngPath = this.$lngPathSub.asObservable();
    this.$proizvodniProgram = this.$proizvodniProgramSub.asObservable();
    this.$certificates = this.$certificatesSub.asObservable();
    this.$tehnologije = this.$tehnologijeSub.asObservable();
    this.$oNama = this.$oNamaSub.asObservable();
    this.$tehkarakteristike = this.$tehKarakteristikeSub.asObservable();
    this.$legal = this.$legalSub.asObservable();
    this.$contact = this.$contactSub.asObservable();
    this.$posao = this.$posaoSub.asObservable();
  }

  setContentData(lng: any) {
    if (lng) {
      switch (lng.code) {
        case 'sr':
          this.$lngPathSub.next('sr');
          this.$proizvodniProgramSub.next(PROIZPROGRAMSR);
          this.$certificatesSub.next(SERTIFIKATISR);
          this.$tehnologijeSub.next(TEHNOLOGIJEPOCETNASR);
          this.$oNamaSub.next(ONAMASR);
          this.$tehKarakteristikeSub.next(TEHKARAKTERISTIKESR2);
          this.$legalSub.next(LEGALSR);
          this.$contactSub.next(CONTACTSR);
          break;
        case 'en': {
          this.$lngPathSub.next('en');
          this.$proizvodniProgramSub.next(PROIZPROGRAMEN);
          this.$certificatesSub.next(SERTIFIKATIEN);
          this.$tehnologijeSub.next(TEHNOLOGIJEPOCETNAEN);
          this.$oNamaSub.next(ONAMAEN);
          this.$tehKarakteristikeSub.next(TEHKARAKTERISTIKEEN2);
          this.$legalSub.next(LEGALEN);
          this.$contactSub.next(CONTACTEN);
          break;
        }
      }
    }
  }
}
