import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

import { isPlatformBrowser } from '@angular/common';

import { Logger } from './logger.service';
import srRS from '../../translations/sr-RS.json';
import enUS from '../../translations/en-US.json';
import frFR from '../../translations/fr-FR.json';
import { Router } from '@angular/router';
import { LangddService } from './langdd.service';

const log = new Logger('I18nService');
const languageKey = 'papirprint_language';

/**
 * Pass-through function to mark a string for translation extraction.
 * Running `npm translations:extract` will include the given string by using this.
 * @param s The string to extract for translation.
 * @return The same string.
 */
export function extract(s: string) {
  return s;
}

@Injectable({
  providedIn: 'root'
})
export class I18nService {
  defaultLanguage!: string;
  supportedLanguages!: string[];

  location: any;

  private langChangeSubscription!: Subscription;
  private seoDataSub$ = new BehaviorSubject({});
  seoData$: Observable<any> = this.seoDataSub$.asObservable();

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject('LOCALSTORAGE') private localStorage: any,
    private translateService: TranslateService,
    private router: Router,
    private lngds: LangddService,
  ) {
    // Embed languages to avoid extra HTTP requests
    translateService.setTranslation('sr-RS', srRS);
    translateService.setTranslation('en-US', enUS);
    translateService.setTranslation('fr-FR', frFR);
  }

  /**
   * Initializes i18n for the application.
   * Loads language from local storage if present, or sets default language.
   * @param defaultLanguage The default language to use.
   * @param supportedLanguages The list of supported languages.
   */
  init(defaultLanguage: string, supportedLanguages: string[]) {
    this.defaultLanguage = defaultLanguage;
    this.supportedLanguages = supportedLanguages;
    this.language = '';
    this.lngds.getLocation();
    // Warning: this subscription will always be alive for the app's lifetime
    this.langChangeSubscription = this.translateService.onLangChange.subscribe(
      (event: LangChangeEvent) => {
        if (isPlatformBrowser(this.platformId)) {
          this.localStorage.setItem(languageKey, event.lang);
        }
      }
    );
  }

  /**
   * Cleans up language change subscription.
   */
  destroy() {
    if (this.langChangeSubscription) {
      this.langChangeSubscription.unsubscribe();
    }
  }

  /**
   * Sets the current language.
   * Note: The current language is saved to the local storage.
   * If no parameter is specified, the language is loaded from local storage (if present).
   * @param language The IETF language code to set.
   */
  set language(language: string) {
    if (isPlatformBrowser(this.platformId)) {
      language =
        language ||
        this.localStorage.getItem(languageKey) ||
        this.translateService.getBrowserCultureLang();
      let isSupportedLanguage = this.supportedLanguages.includes(language);

      // If no exact match is found, search without the region
      if (language && !isSupportedLanguage) {
        language = language.split('-')[0];
        language =
          this.supportedLanguages.find(supportedLanguage =>
            supportedLanguage.startsWith(language)
          ) || '';
        isSupportedLanguage = Boolean(language);
      }

      // Fallback if language is not supported
      if (!isSupportedLanguage) {
        language = this.defaultLanguage;
      }

      log.debug(`Language set to ${language}`);
      this.translateService.use(language);
    }
  }

  /**
   * Gets the current language.
   * @return The current language code.
   */
  get language(): string {
    return this.translateService.currentLang;
  }

  getNewRoutePath() {
    switch (this.language) {
      case 'sr-RS':
        srRS.ROUTES[this.router.url]
          ? this.router.navigateByUrl(srRS.ROUTES[this.router.url])
          : this.router.navigateByUrl(this.router.url);
        break;
      case 'en-US':
        enUS.ROUTES[this.router.url]
          ? this.router.navigateByUrl(enUS.ROUTES[this.router.url])
          : this.router.navigateByUrl(enUS.ROUTES['/en/home']);
        break;
    }
  }

  setSeoData() {
    switch (this.language) {
      case 'sr-RS':
        srRS['PAGES'][this.router.url]
          ? this.seoDataSub$.next(srRS['PAGES'][this.router.url])
          : false;
        break;
      case 'en-US':
        enUS['PAGES'][this.router.url]
          ? this.seoDataSub$.next(enUS['PAGES'][this.router.url])
          : false;
    }
  }

  searchForPage(word: string): any[] | number {
    let result = [];
    if (word && word.toLocaleLowerCase) {
      switch (this.language) {
        case 'sr-RS':
          Object.keys(srRS['PAGES']).map(key => {
            if (result.length < 3) {
              const find = this.searchThrueObject(
                srRS['PAGES'][key],
                key,
                word
              );
              if (find) {
                result.push(find);
              }
            }
          });
          if (result.length === 0) {
            result.push({
              page: 'Ambalaža za prehrambenu indstriju - PAPIR PRINT',
              path: '/sr/proizvodni-program/ambalaza-prehrambena-industrija',
              context:
                'Nismo uspeli da pronađemo sadržaj prema unetom kriterijumu.'
            });
          }
          return result;
        case 'en-US':
           Object.keys(enUS['PAGES']).map(key => {
            if (result.length < 3) {
              const find = this.searchThrueObject(
                enUS['PAGES'][key],
                key,
                word
              );
              if (find) {
                result.push(find);
              }
            }
          });
          if (result.length === 0) {
            result.push({
              page: 'Packaging for food industry - PAPIR PRINT',
              path: '/en/production-program/packaging-food-industry',
              context: 'We could not find any result based on your criteria.'
            });
          }
          return result;
      }
    } else return [''];
  }

  private searchThrueObject(pageObject: any, path: string, word: string) {
    if (pageObject && pageObject['title']) {
      const keys = Object.keys(pageObject);
      for (let i = 0; i < keys.length; i++) {
        const text = pageObject[keys[i]].toLowerCase().trim();
        const index = text.indexOf(word);
        let start = index;
        let end = start + word.length + 15;
        if (index !== -1 && this.router.url !== path) {
          if (text.length < end) {
            end = text.length;
          }
          const fragment = text.substring(start, end);
          return {
            page: pageObject['title'],
            path: path,
            context: pageObject['description'],
            fragment: fragment
          };
        }
      }
      return null;
    }
  }
}
