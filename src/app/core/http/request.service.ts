import { async } from '@angular/core/testing';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable, BehaviorSubject, throwError, of, interval, Subscription } from 'rxjs';
import { map, tap, catchError, switchMap, timeInterval, flatMap, takeWhile } from 'rxjs/operators';
import { environment} from 'src/environments/environment';

import { UserService } from './user.service';

const APPURL = environment.APPURL;

@Injectable({
  providedIn: 'root'
})

export class RequestService {


  private autt_token = '';
  private $isGrantedSub = new BehaviorSubject(false);
  protected http_header: HttpHeaders;
  private refrshSub: Subscription;

  public paginator: IPaginator = {
    per_page: 10,
    page: 0
  };

  // tslint:disable-next-line:max-line-length
  constructor(private http: HttpClient, private us: UserService) {
    this.us.$user.subscribe(user => { if (!user) { this.autt_token = ''; } });
  }

  private setHttpHeader(ctype = 'application/json') {
    this.http_header = new HttpHeaders({
      // 'Content-Type': ctype,
      'Authorization': this.autt_token,
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Credentials': 'true'
    });
  }

  public get(url: string = APPURL, pagination: boolean = true, filter?: any): Observable<any> {
    this.setHttpHeader();
    const httpOptions = { headers: this.http_header, withCredentials: true, params: null };
    if (pagination) {
      // httpOptions.params = this.setPaginatorPar(filter);
    }
    return this.http.get(url, httpOptions).pipe(
      catchError(err => new BehaviorSubject(err['error']).asObservable())
    );
  }

  public post(url: string, body: {}): Observable<any> {
    this.setHttpHeader();
    const httpOptions = { headers: this.http_header, withCredentials: true, params: null };
    return this.http.post(url, body, httpOptions).pipe(
      catchError(err => new BehaviorSubject(err['error']).asObservable())
    );
  }
  
  public postFileUpload(url: string, body: {}): Observable<any> {
    this.setHttpHeader();
    const httpOptions = { 
      headers: this.http_header,
      observe: 'events' as 'events', 
      withCredentials: true, 
      params: null, 
      reportProgress: true,
      };
    return this.http.post(url, body, httpOptions).pipe(
      catchError(err => new BehaviorSubject(err['error']).asObservable())
    );
  }

  public put(url: string, body: {}, id?: number): Observable<any> {
    this.setHttpHeader();
    const httpOptions = { headers: this.http_header, withCredentials: true, params: null };
    return this.http.put(url, body, httpOptions).pipe(
      catchError(err => new BehaviorSubject(err).asObservable())
    );
  }

  public delete(url: string, body: {}): Observable<any> {
    this.setHttpHeader();
    const httpOptions = { headers: this.http_header, withCredentials: true, params: null };
    return this.http.delete(url, body).pipe(
      catchError(err => new BehaviorSubject(err).asObservable())
    );
  }


  public login(loginData: {}): Observable<any> {
    this.http_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'PapirPrint',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS',
    });
    const httpOptions = { headers: this.http_header, withCredential: true };
    return this.http.post(APPURL + '/login', loginData, httpOptions).pipe(
      tap(data => data),
      switchMap((data): Observable<any> => {
        if (data['data']['access_token']) {
          this.autt_token = 'Bearer ' + data['data']['access_token'];
          this.refrshSub = this.refreshToken(data['data']['rt']).subscribe((token) => {
            if (this.autt_token === '') { this.refrshSub.unsubscribe(); }
          });

          return this.get(APPURL + '/user');
        } else { return throwError(data); }
      }),
      catchError((err) => { this.us.logOutUser(); this.autt_token = ''; return new BehaviorSubject(err).asObservable(); })
    );
  }

  public loginClient(loginData: {}): Observable<any> {
    this.http_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'PapirPrint',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Credentials': 'true'
    });
    const httpOptions = { headers: this.http_header, withCredential: true };
    return this.http.post(APPURL + '/clienttoken', loginData, httpOptions).pipe(
      tap(data => data),
      switchMap((data): Observable<any> => {
        if (data['data']['access_token']) {
          this.autt_token = 'Bearer ' + data['data']['access_token'];
          this.refrshSub = this.refreshToken(data['data']['rt']).subscribe((token) => {
            if (this.autt_token === '') { this.refrshSub.unsubscribe(); }
          });
          this.$isGrantedSub.next(true);
          return this.$isGrantedSub.asObservable();
        } else { return throwError(data); }
      }),
      catchError((err) => { this.$isGrantedSub.next(false); this.autt_token = ''; return new BehaviorSubject(err).asObservable(); })
    );
  }

  public logout(): Observable<boolean> {
    this.refrshSub.unsubscribe();
    return this.post(APPURL + '/logout', {});
  }

  public sendResPassReq(email: String) {

    this.http_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Milos',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Credentials': 'true'
    });

    const httpOptions = { headers: this.http_header, withCredential: true };

    return this.http.post(APPURL + '/reqpassres', email, httpOptions).pipe(
      tap(data => data)
    );
  }

  public sendResPass(respass: any) {
    this.http_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Milos',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Credentials': 'true'
    });

    const httpOptions = { headers: this.http_header, withCredential: true };

    return this.http.post(APPURL + '/resetpass', respass, httpOptions).pipe(
      tap(data => data),
      map(data => data),
      catchError(err => new BehaviorSubject(err).asObservable())
    );
  }



  private refreshToken(rt: string): Observable<any> {
    let rft = rt;
    this.setHttpHeader();
    let httpOptions = { headers: this.http_header, withCredentials: true, params: { 'rft': rft } };
    /*
         The call that goes in here will use the existing refresh token to call
         a method on the oAuth server (usually called refreshToken) to get a new
         authorization token for the API calls.
     */
    return interval(110 * 60 * 1000).pipe(
      timeInterval(),
      flatMap(() => this.http.post(APPURL + '/refresh', {}, httpOptions).pipe(
        tap(data => data),
        map(data => {
          if (!data['error'] && data['data']['access_token']) {
            this.autt_token = 'Bearer ' + data['data']['access_token'];
            rft = data['data']['rt'];
            this.setHttpHeader();
            httpOptions = { headers: this.http_header, withCredentials: true, params: { 'rft': rft } };
          }
        })
      )));

  }

  // public setPaginator(paginator: MatPaginator, data: any, first?: boolean) {
  //   console.log(paginator);
  //   if (data['current_page'] && data['per_page'] && data['total']) {
  //     if (paginator) {
  //       first === true ? paginator.firstPage() : false;
  //       paginator.pageSize = data['per_page'];
  //       paginator.length = data['total'];
  //     }
  //   }
  // }

  // public setPaginatorData(data: PageEvent) {
  //   if (data.pageSize !== this.paginator.per_page) {
  //     this.paginator.per_page = data.pageSize;
  //   }
  //   if (data.pageIndex !== this.paginator.page) {
  //     this.paginator.page = data.pageIndex;
  //   }
  // }

  // private setPaginatorPar(filter?: any): HttpParams {
  //   const pag: IPaginator = { per_page: this.paginator.per_page, page: this.paginator.page + 1 };
  //   if (filter) {
  //     return new HttpParams().set('pagination', JSON.stringify(pag))
  //       .set('filter', JSON.stringify(filter));
  //   } else {
  //     return new HttpParams().set('pagination', JSON.stringify(pag));
  //   }
  // }

  // protected setSearhpar(filter: any): HttpParams {
  //   console.log(filter);
  //   return new HttpParams().set('filter', JSON.stringify(filter));
  // }

  // public login(loginData: {}): Observable<any> {
  //   return this.http.post(APPURL + '/login', loginData);
  // }

}

// @Injectable()
// export class RefreshService implements HttpInterceptor {
//   authToken = '';
//   rft = '';
//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable< HttpEvent <any>> {
//       if (this.authToken !== '' && this.rft !== '') {
//         const cloned = req.clone({
//           headers: req.headers.set()
//         });
//       }
//   }
//   setTokens(at: string, rt: string) {
//     this.authToken = at;
//     this.rft = rt;
//   }


// }


export interface IPaginator {
  'per_page': number;
  'page': number;
}
