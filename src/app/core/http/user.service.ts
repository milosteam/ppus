import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

// import { IUserinfo} from '../../shared/models/user';


@Injectable({
  providedIn: 'root'
})

export class UserService {

  private $userSub =  new BehaviorSubject(<any>null);
  public $user = this.$userSub.asObservable();
  constructor() { }

  public setLogedUser(user: any) {
    if (user) {
      this.$userSub.next(user);
    }
  }

  public logOutUser() {
    this.$userSub.next(null);
  }

  public getUserDepartment(): string{
    return this.$userSub.value.department;
  }

}
