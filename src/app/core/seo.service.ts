import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Meta, Title } from '@angular/platform-browser';
import { I18nService } from './i18n.service';

@Injectable({
  providedIn: 'root'
})
export class SeoService {
  private seoDataSub$: BehaviorSubject<null>;

  private titleSufix = 'PAPIR PRINT';

  private metaData = [
    { name: 'author', content: 'Milos Djokovic' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    { 'http-Equiv': 'content-language', content: 'sr-RS' },
    { charset: 'UTF-8' }
  ];

  constructor(
    private meta: Meta,
    private title: Title,
    private i18n: I18nService
  ) {
    this.meta.addTags(this.metaData);
    this.i18n.seoData$.subscribe(data => this.setPageSeo(data));
  }

  private setPageSeo(data) {
    if (data) {
      data.title ? this.title.setTitle(data.title) : 'Nema';
      data.description
        ? this.meta.updateTag(
            { content: data.description },
            "name='description'"
          )
        : false;
      data.keywords
        ? this.meta.updateTag({ content: data.keywords }, "name='keywords'")
        : false;
    }
  }
}
