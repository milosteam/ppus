import { HttpClient } from '@angular/common/http';
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { CookieconsentService } from 'src/app/shell/papirprint/services/cookieconsent.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';

const languageKey = 'papirprint_language';
const geokey = '2cb5a1b13ca5f7268c4888292e6f809c';

@Injectable({
  providedIn: 'root'
})
export class LangddService {
  public $language: Observable<any>;
  public $routeId: Observable<any>;

  private $languageSub = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private cs: CookieconsentService,
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject('LOCALSTORAGE') private localStorage: any
  ) {
    this.$language = this.$languageSub.asObservable();
  }

  public setLanguage(language: any) {
    if (language && language.description) {
      this.$languageSub.next(language);
    }
  }

  // Setovanje jezika na osnovu korisnikove geolokacije
  getLocation() {
    if (isPlatformBrowser(this.platformId)) {
      this.cs.$consentCookie.subscribe(data => {
        if (data) {
          this.http
            .get(
              'http://api.ipstack.com/check?access_key=' +
              geokey +
              '&fields=country_code'
            )
            .subscribe(
              countryCode => {
                if (countryCode && countryCode['country_code'] === 'RS') {
                  this.$languageSub.next({ code: 'sr', description: 'Srpski' });
                } else {
                  this.$languageSub.next({ code: 'en', description: 'English' });
                }
              },
              error => {
                this.setLngFromLocal();
              }
            );
        } else {
          this.setLngFromLocal();
        }
      });
    }
  }

  //Setovanje jezika na osnovu zapisa u lokal storage
  private setLngFromLocal() {
    if (isPlatformBrowser(this.platformId)) {
      if (this.localStorage.getItem(languageKey)) {
        // tslint:disable-next-line: max-line-length
        if (this.localStorage.getItem(languageKey) === 'sr-RS') {
          this.$languageSub.next({ code: 'sr', description: 'Srpski' });
        }
        if (this.localStorage.getItem(languageKey) === 'en-US') {
          this.$languageSub.next({ code: 'en', description: 'English' });
        }
      } else {
        this.$languageSub.next({ code: 'sr', description: 'Srpski' });
      }
    }
  }
}
