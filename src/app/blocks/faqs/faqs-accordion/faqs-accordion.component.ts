import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FaqsService } from '../services/faqs.service';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dc-faqs-accordion',
  templateUrl: './faqs-accordion.component.html',
  styleUrls: ['./faqs-accordion.component.scss'],
  providers: [NgbAccordionConfig] // add the NgbAccordionConfig to the component providers
})
export class FaqsAccordionComponent implements OnInit {
  chevronDown = faChevronDown;
  faqs: Observable<any>;

  constructor(config: NgbAccordionConfig, private cnts: ContentService) {
    config.closeOthers = true;
  }

  ngOnInit() {
    this.faqs = this.cnts.$legal.pipe(
      map(data => data.shortTexts));
  }
}
