import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormControl,
  FormBuilder,
  Validators,
  FormGroupDirective,
  AbstractControl
} from '@angular/forms';
import { CookieconsentService } from 'src/app/shell/papirprint/services/cookieconsent.service';
import { Observable, Subscription } from 'rxjs';
import { ContactFormService } from '../../../shell/services/contact-form.service';
import {
  faSpinner
} from '@fortawesome/free-solid-svg-icons';
import { timer } from 'rxjs';
import { LangddService } from 'src/app/core/langdd.service';


@Component({
  selector: 'dc-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.scss']
})

export class FormContactComponent implements OnInit, OnDestroy {
  
  contactForm = this.fb.group({
    contactEmail: ['', [Validators.required, Validators.email]],
    contactSubject: ['', [Validators.required]],
    contactText: ['', [Validators.required]],
    recaptchaReactive: ['', Validators.required]
  });
  isLoading = false;
  isSent = false;
  isSentSubscrpt: Subscription;
  spinner = faSpinner;
  cs: Observable<boolean>;
  reloadCapthca = false;

  constructor(
    private fb: FormBuilder,
    private cqcs: CookieconsentService,
    private cfs: ContactFormService,
    private lngs: LangddService) { }


  ngOnInit() {
    this.cs = this.cqcs.$consentCookie;
    this.cqcs.$consentCookie.subscribe(data => !data ? this.contactForm.disable() : this.contactForm.enable());
  }

  onContactFromSubmit(contactFrom: FormGroupDirective) {
    if (this.contactForm.invalid) {
      this.toggleInvalidState();
    } else if (this.contactForm.valid) {
      this.isLoading = true;
      this.cfs.sendContatData(this.contactForm.value).subscribe(data => {
        if (!data['error']) {
          this.contactForm.reset();
          contactFrom.reset();
          this.isLoading = false;
          this.isSent = true;
          timer(2000).subscribe(x => {
             this.isSent = false;
          });
        }
      });
    }
  }

  validationState(control: AbstractControl) {
    if (control && !control.pristine) {
      return control.valid ? true : false;
    }
  }
  resolved(event: any) { }

  private toggleInvalidState() {
    Object.keys(this.contactForm.controls).map(key => {
      if (
        this.contactForm.get(key) instanceof FormControl &&
        !this.contactForm.get(key).valid
      ) {
        this.contactForm.get(key).markAsDirty();
      }
    });
  }

  ngOnDestroy(): void {
  }

  isFormDisabled(): boolean {
    if (this.contactForm.status === 'DISABLED') { return true; }
    return false;
  }
}
