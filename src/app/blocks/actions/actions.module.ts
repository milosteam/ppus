import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormContactComponent } from './form-contact/form-contact.component';
import { TranslateModule } from '@ngx-translate/core';
import { RecaptchaModule, RecaptchaFormsModule} from 'ng-recaptcha';


@NgModule({
  declarations: [
    FormContactComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    FontAwesomeModule,
    TranslateModule.forChild({ extend: true })
  ],
  exports: [
    FormContactComponent,
  ],
})
export class ActionsModule {}
