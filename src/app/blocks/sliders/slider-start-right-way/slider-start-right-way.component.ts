import { Component, Input, OnInit } from '@angular/core';
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { ContentService } from 'src/app/core/content.service';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'dc-slider-start-right-way',
  templateUrl: './slider-start-right-way.component.html',
  styleUrls: ['./slider-start-right-way.component.scss']
})
export class SliderStartRightWayComponent implements OnInit {
  SWIPER_CONFIG: SwiperConfigInterface = {
    allowTouchMove: false,
    slidesPerView: 1
  };

  @Input() content;
  path: Observable<string>;
  elements = [
    'Roto Štampa',
    'Flekso Štampa',
    'Laminiranje',
    'Sečenje',
    'Konfekcioniranje',
    'Ekstruzija'
  ];

  longArrowAltRightpp = faLongArrowAltRight;
  currentSlide: number = 0;
  currentKey: string;
  private _data;
  private _keys;

  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.path = this.cnts.$lngPath;
    if (this.content) {
      this._data = this.content[0];
      this._keys = Object.keys(this.content[0]);
    }
  }

  updateSlider(currentSlide) {
    this.currentSlide = currentSlide;
  }

  public onIndexChange(index: number): void {
    this.currentSlide = index;
  }

  public keys() {
    return this._keys;
  }

  public data() {
    return this._data;
  }
}
