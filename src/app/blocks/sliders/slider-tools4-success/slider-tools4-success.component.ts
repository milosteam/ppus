import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { TEHKARAKTERISTIKESR2 } from 'src/app/pptexts/sr/tehnickekarakteristike';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dc-slider-tools4-success',
  templateUrl: './slider-tools4-success.component.html',
  styleUrls: ['./slider-tools4-success.component.scss']
})
export class SliderTools4SuccessComponent implements OnInit, OnDestroy {
  currentSlideHorizontal = 0;

  SWIPER_CONFIG: SwiperConfigInterface = {
    simulateTouch: false
  };

  elements = TEHKARAKTERISTIKESR2.oprema;
  elementsSubscription: Subscription;

  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.elementsSubscription = this.cnts.$tehkarakteristike.subscribe(data => {
      this.elements = data.oprema;
    });
  }

  updateSliderHorizontal(currentSlide) {
    this.currentSlideHorizontal = currentSlide;
  }

  public onIndexChangeHorizontal(index: number): void {
    this.currentSlideHorizontal = index;
  }

  ngOnDestroy(): void {
    this.elementsSubscription.unsubscribe();
  }
}
