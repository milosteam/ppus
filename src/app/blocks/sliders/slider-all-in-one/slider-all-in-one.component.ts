import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dc-slider-all-in-one',
  templateUrl: './slider-all-in-one.component.html',
  styleUrls: ['./slider-all-in-one.component.scss']
})
export class SliderAllInOneComponent implements OnInit {
  categories: Observable<any>;
  path: Observable<string>;
  currentSlide: number = 0;

  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.categories = this.cnts.$proizvodniProgram.pipe(
      map(data => {
        if (data) {
          return Object.keys(data).map(key => [key, data[key].categoryData]);
        }
      })
    );
    this.path = this.cnts.$lngPath;
  }

  updateSlider(currentSlide) {
    this.currentSlide = currentSlide;
  }

  public onIndexChange(index: number): void {
    this.currentSlide = index;
  }
}
