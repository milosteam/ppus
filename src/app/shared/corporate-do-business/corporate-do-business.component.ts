import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { PROIZPROGRAMSR } from 'src/app/pptexts/sr/proizvodniprogram';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'dc-corporate-do-business',
  templateUrl: './corporate-do-business.component.html',
  styleUrls: ['./corporate-do-business.component.scss']
})
export class CorporateDoBusinessComponent implements OnInit {
  path: Observable<string>;

  cards = [
    { img: 'time', title: 'Save time', animation: 'fade-up' },
    { img: 'done', title: 'Get things done', animation: 'fade-up' },
    { img: 'grow', title: 'Grow your business', animation: 'fade-up' },
    { img: 'goals', title: 'Accomplish your goals', animation: 'fade-up' }
  ];
  private _categories = [];
  constructor(private cnts: ContentService, private router: Router) { }

  ngOnInit() {
    this.path = this.cnts.$lngPath;
    this.extractCategories();
  }

  public categories() {
    return this._categories;
  }

  private extractCategories() {
    this.cnts.$proizvodniProgram.subscribe(data => {
      this._categories = [];
      const keys = Object.keys(data);
      keys.forEach(key => {
        this._categories.push({
          link: key,
          data: data[key].categoryData
        });
      });
    });
  }

  onCategryClicked(link: string) {
    this.router.navigateByUrl(link);
  }
}
