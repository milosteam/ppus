import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FeatherModule } from 'angular-feather';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDateParserFormatter, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  Phone,
  Star,
  Send,
  Headphones,
  HardDrive,
  Calendar,
  BarChart,
  DownloadCloud,
  Mail,
  Clipboard,
  CheckSquare,
  Play,
  Box,
  Settings,
  Award,
  Code,
  Camera,
  Sun,
  Sunset,
  Wind,
  Clock,
  Anchor,
  Users,
  Eye,
  Activity,
  UploadCloud,
  PenTool,
  PlusCircle,
  Zap,
  Map,
  Cloud,
  DollarSign,
  ThumbsUp,
  PieChart,
  User,
  LifeBuoy,
  Image,
  Sliders,
  Target,
  File,
  Smartphone,
  MessageCircle,
  CreditCard,
  Book,
  Lock,
  Airplay,
  Monitor,
  Download,
  Hexagon,
  Layers,
  ArrowLeft,
  ArrowRight,
  Repeat,
  Bell
} from 'angular-feather/icons';

import { LoaderComponent } from './loader/loader.component';
import { PageHeaderWaveComponent } from './page-header-wave/page-header-wave.component';
import { FeatherComponent } from './feather/feather.component';
import { BadgeComponent } from './badge/badge.component';
import { BreadcrumbsComponent } from './papirprint/breadcrumbs/breadcrumbs.component';
import { CorporateDoBusinessComponent } from './corporate-do-business/corporate-do-business.component';
import { SaasLightweightTemplateComponent } from './saas-lightweight-template/saas-lightweight-template.component';
import { SaasPerspectiveMockupsComponent } from './saas-perspective-mockups/saas-perspective-mockups.component';
import { DpdComponent } from './papirprint/dpd/dpd.component';
import { DndDirective } from './papirprint/directives/dnd.directive';
import { FileupprogressComponent } from './papirprint/fileupprogress/fileupprogress.component';
import { NgbDateFRParserFormatter } from './papirprint/ngdateformat/customformater';


// Select some icons (use an object, not an array)
const icons = {
  Phone,
  Star,
  Send,
  Headphones,
  HardDrive,
  Calendar,
  CheckSquare,
  BarChart,
  DownloadCloud,
  Mail,
  Clipboard,
  Play,
  PlusCircle,
  Box,
  Settings,
  Award,
  Code,
  Camera,
  Sun,
  Sunset,
  Wind,
  Clock,
  Anchor,
  Users,
  Eye,
  Activity,
  UploadCloud,
  PenTool,
  Zap,
  Map,
  Cloud,
  DollarSign,
  ThumbsUp,
  PieChart,
  User,
  LifeBuoy,
  Image,
  Sliders,
  Target,
  File,
  Smartphone,
  MessageCircle,
  CreditCard,
  Book,
  Lock,
  Airplay,
  Monitor,
  Download,
  Hexagon,
  Layers,
  ArrowLeft,
  ArrowRight,
  Repeat,
  Bell
};

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FeatherModule.pick(icons),
    FontAwesomeModule,
    TranslateModule.forChild({ extend: true }),
    NgbTooltipModule,
    NgbDatepickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoaderComponent,
    PageHeaderWaveComponent,
    FeatherComponent,
    BadgeComponent,
    BreadcrumbsComponent,
    CorporateDoBusinessComponent,
    SaasLightweightTemplateComponent,
    SaasPerspectiveMockupsComponent,
    DpdComponent,
    DndDirective,
    FileupprogressComponent,
  ],
  exports: [
    LoaderComponent,
    PageHeaderWaveComponent,
    FeatherComponent,
    BadgeComponent,
    BreadcrumbsComponent,
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    CorporateDoBusinessComponent,
    SaasLightweightTemplateComponent,
    SaasPerspectiveMockupsComponent,
    NgbTooltipModule,
    DpdComponent,
  ],

  providers:[{
    provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter
  }]

})

export class SharedModule { }


