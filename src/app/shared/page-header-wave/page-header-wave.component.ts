import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { ContentRef } from '@ng-bootstrap/ng-bootstrap/util/popup';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-page-header-wave',
  templateUrl: './page-header-wave.component.html',
  styleUrls: ['./page-header-wave.component.scss']
})
export class PageHeaderWaveComponent implements OnInit {
  @Input() rows: number;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() fill: string;
  @Input() containerClass: string;

  path: Observable<string>;
  constructor(private cnts: ContentService) {}

  ngOnInit() {
    this.path = this.cnts.$lngPath;
  }

  getShapeFill(): string {
    return this.fill || 'shape-fill-contrast';
  }
}
