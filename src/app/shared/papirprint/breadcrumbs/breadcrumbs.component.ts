import { InvokeFunctionExpr } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  NavigationStart,
  Router
} from '@angular/router';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'dc-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input() category;
  @Input() product;
  private _previusLink = { url: null, label: null };
  private _currentLink = { url: null, label: null };

  constructor(private router: Router) {}

  ngOnInit(): void {
    if (this.product && this.category) {
      this._previusLink.url = this.router.url.substr(
        0,
        this.router.url.lastIndexOf('/')
      );
      this._previusLink.label = this.category.categoryData.categoryNameShort;
      this._currentLink.url = this.router.url;
      this._currentLink.label = this.product.productName;
    }
  }

  goBack(even: Event) {
    event.preventDefault();
    this.router.navigateByUrl(this._previusLink.url);
  }

  public previusLink() {
    return this._previusLink;
  }

  public currentLink() {
    return this._currentLink;
  }
}
