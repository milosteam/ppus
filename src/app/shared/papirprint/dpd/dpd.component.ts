import { Component, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-dpd',
  templateUrl: './dpd.component.html',
  styleUrls: ['./dpd.component.scss']
})


export class DpdComponent implements OnInit, OnChanges {

  @Output() onDrop: EventEmitter<any[]> = new EventEmitter();
  @Input() fileIndex: number;
  @Input() fileProgres: number;

  fileUpload = new FormControl();
  files: any[] = [];
  fuperror = false;

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.fileProgres === undefined) { this.fileProgres = 10; }
    this.uploadFilesSimulator(this.fileProgres);
  }

  ngOnInit(): void {
    this.fileProgres = 0;
  }

  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      if (this.files.length >= 1) { this.fuperror = false; }
      else {
        const fileSize = item.size / 1024 / 1024;
        const extension = item.name.substr((item.name.lastIndexOf('.') + 1));
        if (fileSize > 2 || extension !== "pdf") {
          this.fuperror = true;
        } else {
          this.fuperror = false;
          item.progress = 0;
          this.files.push(item);
          this.onDrop.emit(this.files);
          this.fileUpload.reset();
        }
      }
    }
    // this.uploadFilesSimulator(0);
  }



  uploadFilesSimulator(progress) {
    if (this.files.length === 0) return;
    if (this.fileProgres !== 101) {
      this.files[this.fileIndex].progress = progress;
    }
    else {
      this.files.splice(this.fileIndex, 1);
    }
  }



  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  deleteFile(index: number) {
    this.files.splice(index, 1);
    this.onDrop.emit(this.files);
    this.fileUpload.reset();
  }

}
