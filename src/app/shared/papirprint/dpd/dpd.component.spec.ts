import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DpdComponent } from './dpd.component';

describe('DpdComponent', () => {
  let component: DpdComponent;
  let fixture: ComponentFixture<DpdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DpdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DpdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
