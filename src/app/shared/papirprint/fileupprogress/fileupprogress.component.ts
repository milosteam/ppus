import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-fileupprogress',
  templateUrl: './fileupprogress.component.html',
  styleUrls: ['./fileupprogress.component.scss']
})
export class FileupprogressComponent implements OnInit {
  @Input() progress = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
