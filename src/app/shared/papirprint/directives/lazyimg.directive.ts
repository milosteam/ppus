import { Directive, ElementRef, HostListener } from '@angular/core';


@Directive({
  selector: '[pplazy]'
})
export class LazyimgDirective {

  constructor({ nativeElement }: ElementRef<HTMLImageElement>) {
    const parentEl= nativeElement.parentElement.parentElement;
    if (parentEl.hasAttribute("data-aos")){
      
    }
    const supports = 'loading' in HTMLImageElement.prototype;
    if (supports) {
      nativeElement.setAttribute('loading', 'lazy');
      nativeElement.addEventListener('load',()=>{
      });
    }
  }

}
