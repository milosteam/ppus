import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosaoComponent } from './components/posao/posao.component';
import { SliderStartRightWayComponent } from './components/slider-start-right-way/slider-start-right-way.component';
import { AppPosaoRoutingModule } from './app-posao-routing.module';
import { ShellModule } from 'src/app/shell/shell.module';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SharedModule } from 'src/app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { RecaptchaModule, RecaptchaFormsModule} from 'ng-recaptcha';

@NgModule({
  declarations: [PosaoComponent, SliderStartRightWayComponent],
  imports: [
    CommonModule,
    AppPosaoRoutingModule,
    SharedModule,
    ShellModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ]
})
export class PosaoModule { }
