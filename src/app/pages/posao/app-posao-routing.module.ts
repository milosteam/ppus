import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { PosaoComponent } from './components/posao/posao.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: PosaoComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AppPosaoRoutingModule {}
