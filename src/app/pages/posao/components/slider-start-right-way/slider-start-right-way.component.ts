import { Component, OnInit } from '@angular/core';
import { faLongArrowAltRight, faSpinner } from '@fortawesome/free-solid-svg-icons';


import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective,
  AbstractControl,
  FormArray
} from '@angular/forms';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Observable } from 'rxjs';
import { CookieconsentService } from 'src/app/shell/papirprint/services/cookieconsent.service';
import { HostListener } from '@angular/core';
import { JobFormService } from 'src/app/shell/services/job-form.service';
import { HttpEventType } from '@angular/common/http';
import { timer } from 'rxjs';

@Component({
  selector: 'dc-slider-start-right-way',
  templateUrl: './slider-start-right-way.component.html',
  styleUrls: ['./slider-start-right-way.component.scss']
})
export class SliderStartRightWayComponent implements OnInit {

  isLoading = false;
  spinner = faSpinner;

  isSent = false;
  fileOnUpload = { index: null, progress: 10 };

  cs: Observable<boolean>;
  reloadCapthca = false;


  SWIPER_CONFIG: SwiperConfigInterface = {
    simulateTouch: false
  };

  elements = ['Lični podaci', 'Radno iskustvo', 'Stepen Obrazovanja', 'Curriculum Vitae'];
  longArrowAltRight = faLongArrowAltRight;
  currentSlide: number = 0;

  vrsteAngazovanja = [
    'Izaberite',
    'Zaposlen u kompaniji',
    'Praksa u kompaniji',
    'Obuka u kompaniji'
  ];

  sluzbe = [
    'Izaberite',
    'U proizvodnji - proces štampe',
    'U proizvodnji - proces kaširanja',
    'U proizvodnji - proces sečenja',
    'U proizvodnji - proces konfekcioniranja',
    'U proizvodnji - proces ekstruzije',
    'Izrada štamparske forme - izrada št. valjaka',
    'Izrada štamparske forme - izrada klišea',
    'Rad u magacinu',
    'Tehnologija, kontrola kvaliteta, laboratorija',
    'Financije, nabavka, planiranje ili komercijala',
    'Operativna priprema, tehnička priprema, grafička priprema',
    'Informacione tehnologije, Održavanje, Transport',
    'Ostalo'
  ];

  status = [
    'Izaberite',
    'U toku',
    'Završio / la'
  ]

  cnt = 'Srbija';

  pretPosaoInfo = {
    opisPosla: { value: '', disabled: false, validation: Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$') },
    nazivFirme: { value: '', disabled: false, validation: Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$') },
    tipZaposlenja: { value: this.vrsteAngazovanja[0], disabled: false },
    datumOd: { value: '', disabled: false, validation: Validators.pattern('^[12][0-9]{3}$') },
    datumDo: { value: '', disabled: false, validation: Validators.pattern('^[12*][0-9,*]{3}$') }
  };

  obrazInfo = {
    diploma: { value: '', disabled: false, validation: Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$') },
    obrazovnaUstanova: { value: '', disabled: false, validation: Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$') },
    status: { value: this.status[0], disabled: false }
  };

  jobForm = this.fb.group({
    imePrezime: ['', [Validators.required, Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$')]],
    datumRodjenja: ['', [Validators.required]],
    ulica: ['', [Validators.required, Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ10-9\\s\\-,.]{3,})$')]],
    postalCode: ['', [Validators.required, Validators.pattern('^[123][0-9]{4}$')]],
    grad: ['', [Validators.required, Validators.pattern('^([a-zA-ZšŠđĐžŽčČćĆ\\s]{2,})$')]],
    drzava: [this.cnt, [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phoneNumber: ['', [Validators.required]],
    position: [this.sluzbe[0]],
    recaptchaReactive: ['', Validators.required],
    radnoIskustvo: this.fb.array([
      this.objectToFormControl(this.pretPosaoInfo)
    ]),
    obrazovanje: this.fb.array([
      this.objectToFormControl(this.obrazInfo)
    ]),
    katImage: this.fb.array([
    ])
  });

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.keyCode === 9 && event.key === 'Tab') {
      if ((<Object>event.target) instanceof HTMLElement && (<HTMLElement>event.target).getAttribute("name")) {
        {
          const elName = (<HTMLElement>event.target).getAttribute("name");
          if (elName === 'job-do' || elName === 'job_position' || elName === 'job-status') {
            event.preventDefault();
            event.stopPropagation();
          }
        }
      }
    }
  }

  constructor(
    private fb: FormBuilder,
    private cqcs: CookieconsentService,
    private jfs: JobFormService
  ) { }

  ngOnInit() {
    this.cs = this.cqcs.$consentCookie;
    this.cqcs.$consentCookie.subscribe(data => !data ? this.jobForm.disable() : this.jobForm.enable());
  }

  updateSlider(currentSlide) {
    this.currentSlide = currentSlide;
  }

  public onIndexChange(index: number): void {
    this.currentSlide = index;
  }

  protected objectToFormControl(object: any): FormGroup {
    try {
      const formGroup = new FormGroup({});
      Object.keys(object).forEach(key => {
        if (object[key] && object[key].value && object[key].disabled) {
          // tslint:disable-next-line:max-line-length
          formGroup.addControl(key, new FormControl({
            value: object[key].value,
            disabled: object[key].disabled ? object[key].disabled : false
          }, [Validators.required]));
        } else if (object[key] !== '') {
          formGroup.addControl(key, new FormControl({
            value: object[key].value,
            disabled: object[key].disabled ? object[key].disabled : false
          }));
          if (object[key].validation) {
            formGroup.get(key).setValidators(object[key].validation);
          }
        }
      });
      return formGroup;
    }
    catch (err) {
    }

  }

  addToFormArray(object: any, formArry: FormArray | AbstractControl): void {
    if ((<FormArray>formArry).length < 10) {
      (<FormArray>formArry).push(this.objectToFormControl(object));
    }
  }

  removeFromArr(formArry: FormArray, i: number): void {
    console.dir(formArry);
    if (formArry instanceof FormArray && formArry.controls[i]) {
      formArry.removeAt(i);
    }
  }

  markAsUndirty(control: FormControl) {
    if (control instanceof FormControl) {
      if (control.value === '' || control.value === 'Izaberi') {
        control.markAsPristine({ onlySelf: true });
      }
    }
  }

  public returnFormArray(formArray: FormArray | AbstractControl) {
    if (formArray && formArray instanceof FormArray) {
      return <FormArray>formArray;
    }
  }

  public getFormArrayGroup(arrayName: string, index: number) {
    return this.jobForm.get(arrayName).get(index.toString());
  }

  public getArrayControl(arrayName: string, index: number) {
  }

  public onJobFormSubmit(jobForm?: FormGroupDirective) {
    this.jobForm.get('position').value === 'Izaberite' ? this.jobForm.get('position').setErrors({ 'incorrect': true }) : false;
    if (this.jobForm.invalid) {
      this.toggleInvalidState(this.jobForm);
      
    } else if (this.jobForm.valid) {
      this.isLoading = true;
      this.jobForm.get('datumRodjenja').setValue(
        this.dateToTimeStamp(this.jobForm.get('datumRodjenja').value)
      );
      let DATA = this.jsonToFormData(this.jobForm);
      DATA.append('radnoIskustvo', JSON.stringify(this.jobForm.get('radnoIskustvo').value));
      DATA.append('obrazovanje', JSON.stringify(this.jobForm.get('obrazovanje').value));
      this.jfs.sendCvData(DATA).subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.fileOnUpload.progress = Math.round(100 * event.loaded / event.total);
          }
          if (event.type === HttpEventType.Response) {
            if (!event.body['error']) {
              this.isSent = true;
              timer(2000).subscribe(x => {
                jobForm.reset();
                this.jobForm.reset();
                this.fileOnUpload = { index: null, progress: 101 };
                this.isSent = false;
                this.isLoading = false;
                this.jobForm.get('drzava').setValue(this.cnt);
                this.jobForm.get('position').setValue(this.sluzbe[0]);
              });
            }
          } else if (event instanceof ProgressEvent && event.type === 'error'){
            jobForm.reset();
            this.jobForm.reset();
            this.isSent = false;
            this.isLoading = false;
            this.jobForm.get('drzava').setValue(this.cnt);
            this.jobForm.get('position').setValue(this.sluzbe[0]);
          }
        });
    }
  }

  onDrop(files: any[]) {
    if (files.length > 0) {
      files.map((file, index) => {
        if (file instanceof File) {
          this.fileOnUpload.index = index;
          this.fileOnUpload.progress = 10;
          this.createImage(<FormArray>this.jobForm.get('katImage'), file);
          console.log(this.jobForm);
        }
      })
    }
    else {
      (<FormArray>this.jobForm.get('katImage')).clear();
      (<FormArray>this.jobForm.get('katImage')).reset();
    }
  }

  protected createImage(formImageArray: FormArray, image: File): void {
    const images = formImageArray as FormArray;
    formImageArray.clear();
    formImageArray.reset();
    const newimage = new FormControl();
    newimage.setValue(image);
    images.push(newimage);
  }

  protected dateToTimeStamp(date: any): string {
    if (date.year && date.month && date.day) {
      const tmpDate = new Date('' + date.year + '-' + date.month + '-' + date.day);
      // tslint:disable-next-line:max-line-length
      return tmpDate.getUTCFullYear() + '-' + this.twoDigits((tmpDate.getUTCMonth() + 1)) + '-' + this.twoDigits(tmpDate.getUTCDate()) + ' ' + '00' + ':' + '00' + ':' + '00';
    }
  }

  private twoDigits(d) {
    if (0 <= d && d < 10) { return "0" + d.toString(); }
    if (-10 < d && d < 0) { return "-0" + (-1 * d).toString(); }
    return d.toString();
  }

  protected jsonToFormData(object: FormGroup): FormData {
    let data = new FormData;
    appendFormVontroll(data, object);
    function appendFormVontroll(data: FormData, object: FormGroup | FormArray, arr?: string) {
      for (const control in object.controls) {
        if (!(object.controls[control] instanceof FormArray) && object.controls[control].valid) {
          arr ?
            data.append(arr + '[]', object.controls[control].value) :
            data.append(control, object.controls[control].value);
        }
        else if (object.controls[control] instanceof FormArray) {
          appendFormVontroll(data, object.controls[control], control);
        }
      }
    }
    return data;
  }

  private toggleInvalidState(control?: AbstractControl) {
    if (control instanceof FormArray || control instanceof FormGroup) {
      Object.keys(control.controls).map(key => {
        this.toggleInvalidState(control.controls[key])
      });
    }
    else if (control instanceof FormControl && !control.valid) {
      if (control.value === "Izaberite") { control.markAsDirty(); }
      control.markAsDirty();
    }
  }


  isFormDisabled(): boolean {
    if (this.jobForm.status === 'DISABLED') { return true; }
    return false;
  }

  validationState(control: AbstractControl) {
    if (control && !control.pristine) {
      return control.valid ? true : false;
    }
  }

  checkValidity(control: string, array?: FormArray) {
    if (!array) {
    }
  }

  resolved(event: any) { }
}
