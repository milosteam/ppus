import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ContentService } from 'src/app/core/content.service';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'app-posao',
  templateUrl: './posao.component.html',
  styleUrls: ['./posao.component.scss']
})
export class PosaoComponent implements OnInit, AfterViewInit {

  $posao: Observable<any>;

  constructor(private cnts: ContentService, private logos: LogoService) { }

  ngOnInit(): void {
    this.logos.setLogo(false);
    this.$posao = this.cnts.$posao;
  }

  ngAfterViewInit(): void {
    this.logos.setPageIsLoading(false);
  }

}
