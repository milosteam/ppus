import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from 'src/app/core';
import { HomeComponent } from './components/home/home.component';
import { Shell } from 'src/app/shell/services/shell.service';
import { ContentService } from 'src/app/core/content.service';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/sr/pocetna', pathMatch: 'full' },
    {
      path: 'home1',
      component: HomeComponent,
      data: { title: extract('Home') }
    }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule {}
