import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { ShellModule } from 'src/app/shell/shell.module';
import { AppPocetnaRoutingModule } from './app-pocetna-routing.module';
import { PocetnaComponent } from './components/pocetna/pocetna.component';
import { PocetnaLanding2HeadingComponent } from './components/pocetna-landing2-heading/pocetna-landing2-heading.component';
import { PocetnaLanding2FeaturesComponent } from './components/pocetna-landing2-features/pocetna-landing2-features.component';
import { PocetnaLanding2TrendingDesignComponent } from './components/pocetna-landing2-trending-design/pocetna-landing2-trending-design.component';
import { PocetnaLanding2SafetyComponent } from './components/pocetna-landing2-safety/pocetna-landing2-safety.component';

@NgModule({
  declarations: [
    PocetnaComponent,
    PocetnaLanding2HeadingComponent,
    PocetnaLanding2FeaturesComponent,
    PocetnaLanding2TrendingDesignComponent,
    PocetnaLanding2SafetyComponent
  ],
  imports: [
    CommonModule,
    AppPocetnaRoutingModule,
    SharedModule,
    SlidersModule,
    ShellModule
  ]
})
export class PocetnaModule {}
