import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { PocetnaComponent } from './components/pocetna/pocetna.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: PocetnaComponent,
    data: { title: extract('App Landing 2') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AppPocetnaRoutingModule {}
