import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { faChartLine, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-pocetna-landing2-heading',
  templateUrl: './pocetna-landing2-heading.component.html',
  styleUrls: ['./pocetna-landing2-heading.component.scss']
})
export class PocetnaLanding2HeadingComponent implements OnInit {
  lightbulb = faChartLine;
  search = faSearch;
  path: Observable<string>;
  shapes: object[] = [
    { aos: 'fade-down-right', duration: '1500', delay: '100' },
    { aos: 'fade-down', duration: '1000', delay: '100' },
    { aos: 'fade-up-left', duration: '500', delay: '200' },
    { aos: 'fade-up', duration: '500', delay: '200' }
  ];
  constructor(private cnts: ContentService) {}

  ngOnInit(): void {
    this.path = this.cnts.$lngPath;
  }
}
