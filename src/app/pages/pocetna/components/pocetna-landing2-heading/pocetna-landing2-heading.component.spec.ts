import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PocetnaLanding2HeadingComponent } from './pocetna-landing2-heading.component';

describe('PocetnaLanding2HeadingComponent', () => {
  let component: PocetnaLanding2HeadingComponent;
  let fixture: ComponentFixture<PocetnaLanding2HeadingComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PocetnaLanding2HeadingComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PocetnaLanding2HeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
