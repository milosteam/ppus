import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-pocetna-landing2-features',
  templateUrl: './pocetna-landing2-features.component.html',
  styleUrls: ['./pocetna-landing2-features.component.scss']
})
export class PocetnaLanding2FeaturesComponent implements OnInit {
  features: Observable<any>;

  constructor(private cnts: ContentService) {}

  ngOnInit(): void {
    this.features = this.cnts.$certificates;
  }
}
