import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PocetnaLanding2FeaturesComponent } from './pocetna-landing2-features.component';

describe('PocetnaLanding2FeaturesComponent', () => {
  let component: PocetnaLanding2FeaturesComponent;
  let fixture: ComponentFixture<PocetnaLanding2FeaturesComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PocetnaLanding2FeaturesComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PocetnaLanding2FeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
