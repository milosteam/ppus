import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-pocetna-landing2-trending-design',
  templateUrl: './pocetna-landing2-trending-design.component.html',
  styleUrls: ['./pocetna-landing2-trending-design.component.scss']
})
export class PocetnaLanding2TrendingDesignComponent implements OnInit {
  path: Observable<string>;
  constructor(private cnts: ContentService) {}

  ngOnInit(): void {
    this.path = this.cnts.$lngPath;
  }
}
