import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PocetnaLanding2TrendingDesignComponent } from './pocetna-landing2-trending-design.component';

describe('PocetnaLanding2TrendingDesignComponent', () => {
  let component: PocetnaLanding2TrendingDesignComponent;
  let fixture: ComponentFixture<PocetnaLanding2TrendingDesignComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PocetnaLanding2TrendingDesignComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PocetnaLanding2TrendingDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
