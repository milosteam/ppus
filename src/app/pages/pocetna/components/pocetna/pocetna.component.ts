import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LangddService } from 'src/app/core/langdd.service';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-pocetna',
  templateUrl: './pocetna.component.html',
  styleUrls: ['./pocetna.component.scss']
})
export class PocetnaComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor(private lngs: LangddService, private logos: LogoService) { }
  ngOnDestroy(): void {
    this.logos.setLogo(false);
  }
  ngOnInit(): void {
    this.logos.setLogo(true);
  }
  ngAfterViewInit(): void {
    this.logos.setPageIsLoading(false);
  }

}
