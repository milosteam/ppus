import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-pocetna-landing2-safety',
  templateUrl: './pocetna-landing2-safety.component.html',
  styleUrls: ['./pocetna-landing2-safety.component.scss']
})
export class PocetnaLanding2SafetyComponent implements OnInit {
  items: Observable<any>;
  path: Observable<any>;

  cards: object[] = [
    { class: 'mt-md-6' },
    { class: '' },
    { class: 'mt-md-6' },
    { class: 'mx-auto mt-md-4n' }
  ];
  constructor(private cnts: ContentService) {}

  ngOnInit(): void {
    this.path = this.cnts.$lngPath;
    this.items = this.cnts.$tehnologije;
  }
}
