import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PocetnaLanding2SafetyComponent } from './pocetna-landing2-safety.component';

describe('PocetnaLanding2SafetyComponent', () => {
  let component: PocetnaLanding2SafetyComponent;
  let fixture: ComponentFixture<PocetnaLanding2SafetyComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PocetnaLanding2SafetyComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PocetnaLanding2SafetyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
