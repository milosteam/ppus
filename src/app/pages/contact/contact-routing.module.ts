import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { ContactComponent } from './components/contact/contact.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: ContactComponent,
    data: { title: extract('About') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ContactRoutingModule {}
