import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, AfterViewInit {
  contact: Observable<any>;

  // google maps zoom level
  zoom: number = 14;

  // initial center position for the map
  lat: number = 44.02612795053626;
  lng: number = 20.46921598255174;
  gestureHandling = "none";

  marker: marker =
    {
      lat: 44.02612795053626,
      lng: 20.46921598255174,
      label: 'PP',
      draggable: false
    }

  constructor(private cnts: ContentService, private logos: LogoService) { }
  ngAfterViewInit(): void {
    this.logos.setPageIsLoading(false);
  }

  ngOnInit() {
    this.contact = this.cnts.$contact;
    this.logos.setLogo(false);
  }

  onZoomChange(event: any) {
   this.zoom = event as number; 
  }

  showMarker() {
    return this.zoom < 16 ? true : false;
  }
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
