import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { ActionsModule } from 'src/app/blocks/actions/actions.module';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { ShellModule } from 'src/app/shell/shell.module';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './components/contact/contact.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactOtherChannelsComponent } from './components/contact-other-channels/contact-other-channels.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    ContactComponent,
    ContactFormComponent,
    ContactOtherChannelsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ShellModule,
    ActionsModule,
    ContactRoutingModule,
    SlidersModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAlVgGdRs7PfPnRG_dl1QI_o_14dK2EDmA'
    }),
  ]
})
export class ContactModule {}
