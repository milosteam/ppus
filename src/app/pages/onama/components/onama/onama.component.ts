import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-onama',
  templateUrl: './onama.component.html',
  styleUrls: ['./onama.component.scss']
})
export class OnamaComponent implements OnInit, AfterViewInit {
  path: Observable<any> = this.cnts.$lngPath;
  profilKompanije: Observable<any>;
  play = faPlay;

  constructor(
    private cnts: ContentService, 
    private cd: ChangeDetectorRef,
    private logos: LogoService
    ) {}

  ngAfterViewInit(): void {
    this.cd.detectChanges();
    this.logos.setPageIsLoading(false);
  }

  ngOnInit(): void {
    this.path = this.cnts.$lngPath;
    this.profilKompanije = this.cnts.$oNama;
    this.logos.setLogo(false);
  }

}
