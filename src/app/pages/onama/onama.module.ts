import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { ShellModule } from 'src/app/shell/shell.module';
import { UsualModule } from 'src/app/blocks/usual/usual.module';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { AppOnamaRoutingModule } from './app-onama-routing.module';
import { OnamaComponent } from './components/onama/onama.component';


@NgModule({
  declarations: [OnamaComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShellModule,
    UsualModule,
    AppOnamaRoutingModule,
    SlidersModule,
  ]
})
export class OnamaModule {}
