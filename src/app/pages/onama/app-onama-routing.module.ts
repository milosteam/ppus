import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from '../../core';

import { OnamaComponent } from './components/onama/onama.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: OnamaComponent,
    data: { title: extract('O nama') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AppOnamaRoutingModule {}
