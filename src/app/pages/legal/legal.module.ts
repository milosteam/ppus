import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { ShellModule } from 'src/app/shell/shell.module';
import { LegalRoutingModule } from './legal-routing.module';
import { FaqsModule } from 'src/app/blocks/faqs/faqs.module';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { LegalComponent } from './components/legal/legal.component';

@NgModule({
  declarations: [LegalComponent],
  imports: [
    CommonModule,
    LegalRoutingModule,
    ShellModule,
    SharedModule,
    FaqsModule,
    SlidersModule
  ]
})
export class LegalModule {}
