import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ContentService } from 'src/app/core/content.service';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit, AfterViewInit {
  legal: Observable<any>;
  constructor(private cnts: ContentService, private logos: LogoService) { }

  ngOnInit(): void {
   this.logos.setLogo(false);
   this.legal = this.cnts.$legal;
  }

  ngAfterViewInit(): void {
    this.logos.setPageIsLoading(false);
  }
}
