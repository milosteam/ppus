import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { LegalComponent } from './components/legal/legal.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: LegalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class LegalRoutingModule {}
