import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellModule } from 'src/app/shell/shell.module';
import { SharedModule } from 'src/app/shared';

import { AppProductsRoutingModule } from './app-products-routing.module';
import { ProductsComponent } from './components/products/products.component';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { ProductFeaturesComponent } from './components/product-features/product-features.component';
import { SwiperModule } from 'ngx-swiper-wrapper';


@NgModule({
  declarations: [ProductsComponent, ProductFeaturesComponent],
  imports: [
    CommonModule,
    AppProductsRoutingModule,
    ShellModule,
    SharedModule,
    SwiperModule,
    SlidersModule,
  ]
})
export class ProductsModule {}
