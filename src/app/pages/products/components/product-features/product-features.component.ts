import { Component, Input, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { Observable } from 'rxjs';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-product-features',
  templateUrl: './product-features.component.html',
  styleUrls: ['./product-features.component.scss']
})
export class ProductFeaturesComponent implements OnInit {
  @Input() product;
  path: Observable<string>;
  currentSlide: number = 0;
  features = [
    {
      title: 'Perfect for modern and growing Apps & Startups',
      icon: 'arrow-right'
    },
    {
      title: 'Predesigned growing set of modern web components',
      icon: 'arrow-right'
    },
    {
      title: 'Modern & eye-catching design to enchant your visitors',
      icon: 'arrow-right'
    },
    {
      title: "Focus on your business, don't worry about your website",
      icon: 'arrow-right'
    }
  ];

  elements: object[] = [
    { icon: 'image', title: 'Vizuelnu izgled - vrsta štampe' },
    { icon: 'sunset', title: 'Struktura materijala - barijerne osobine' },
    { icon: 'check-square', title: 'Zdravstvena ispravnost - kvalitet' },
    { icon: 'plus-circle', title: 'Premium dodaci' }
  ];

  constructor(
    private cnts: ContentService,
    private logo: LogoService) { }

  ngOnInit() {
    this.path = this.cnts.$lngPath;
    console.log('Tu sam');
  }

  updateSlider(currentSlide) {
    this.currentSlide = currentSlide;
  }

  public onIndexChange(index: number): void {
    this.currentSlide = index;
  }
}
