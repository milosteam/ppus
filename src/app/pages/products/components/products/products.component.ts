import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ContentService } from 'src/app/core/content.service';
import { PROIZPROGRAMSR } from 'src/app/pptexts/sr/proizvodniprogram';
import { map } from 'rxjs/operators';

@Component({
  selector: 'dc-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private _category;
  private _product;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cnts: ContentService
  ) {}

  ngOnInit(): void {
    this.setPageData();
  }

  public category() {
    return this._category;
  }

  public product() {
    return this._product;
  }

  private setPageData() {
    const routeData = this.router.url.split('/');
    const product = routeData[4];
    const category =
      '/' + routeData[1] + '/' + routeData[2] + '/' + routeData[3];
    this.cnts.$proizvodniProgram.subscribe(data => {
      if (data && data[category].categoryProducts[product]) {
        this._category = data[category];
        this._product = data[category].categoryProducts[product];
      } else {
        // console.log(PROIZPROGRAMSR[category]);
        // console.log(product);
        // this.router.navigateByUrl('home');
      }
    });
  }
}
