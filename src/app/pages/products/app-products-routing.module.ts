import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from '../../core';

import { ProductsComponent } from './components/products/products.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: ProductsComponent,
    data: { title: extract('O nama') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AppProductsRoutingModule {}
