import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from 'src/app/core';
import { OnlinePaymentComponent } from './components/online-payment/online-payment.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: OnlinePaymentComponent,
    data: { animtion: 'fader' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnlinePaymentRoutingModule {}
