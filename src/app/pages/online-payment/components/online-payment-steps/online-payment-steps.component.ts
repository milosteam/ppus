import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentService } from 'src/app/core/content.service';
import {
  faUser,
  faUsersCog,
  faUserShield,
  faUserTag
} from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';

@Component({
  selector: 'dc-online-payment-steps',
  templateUrl: './online-payment-steps.component.html',
  styleUrls: ['./online-payment-steps.component.scss']
})
export class OnlinePaymentStepsComponent implements OnInit {
  @Input() products: any;

  public icon = faUser;
  path: Observable<string>;

  steps = [
    {
      icon: faUser,
      image: { name: 'register', class: 'w-50' },
      title: 'register free',
      button: { text: 'Sign Up Free' }
    },
    {
      icon: faUsersCog,
      image: { name: 'configure' },
      title: 'configure your sourcing',
      button: { text: 'Learn More' }
    },
    {
      icon: faUserShield,
      image: { name: 'receive' },
      title: 'start receiving payments',
      button: { text: 'Learn More' }
    }
  ];

  constructor(private router: Router, private cnst: ContentService) {}

  ngOnInit() {
    this.path = this.cnst.$lngPath;
  }

  public category() {
    return this.router.url;
  }
}
