import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentService } from 'src/app/core/content.service';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-online-payment',
  templateUrl: './online-payment.component.html',
  styleUrls: ['./online-payment.component.scss']
})
export class OnlinePaymentComponent implements OnInit, AfterViewChecked {
  private _category;
  private _products;

  constructor (
    private router: Router,
    private cnts: ContentService,
    private logos: LogoService
  ) { }
  
  ngOnInit() {
    this.setPageData();
    this.logos.setLogo(false);
  }

  ngAfterViewChecked(): void {
    this.logos.setPageIsLoading(false);
  }

  public category(): any {
    return this._category;
  }

  public products(): any {
    return this._products;
  }

  private setPageData() {
    this.cnts.$proizvodniProgram.subscribe(data => {
      if (data && data[this.router.url]) {
        this._category = data[this.router.url].categoryData;
        this._products = data[this.router.url].categoryProducts;
        this.onActivate();
      }
    });
  }

  private onActivate() {
    // const scrollToTop = window.setInterval(() => {
    //   const pos = window.pageYOffset;
    //   if (pos > 0) {
    //     window.scrollTo(0, pos - 20); // how far to scroll on each step
    //   } else {
    //     window.clearInterval(scrollToTop);
    //   }
    // }, 5);
  }
}
