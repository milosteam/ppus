import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared';
import { CtaModule } from 'src/app/blocks/cta/cta.module';
import { TestimonialsModule } from 'src/app/blocks/testimonials/testimonials.module';
import { UsualModule } from 'src/app/blocks/usual/usual.module';
import { ActionsModule } from 'src/app/blocks/actions/actions.module';
import { ShellModule } from 'src/app/shell/shell.module';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { OnlinePaymentRoutingModule } from './online-payment-routing.module';
import { OnlinePaymentComponent } from './components/online-payment/online-payment.component';
import { OnlinePaymentStepsComponent } from './components/online-payment-steps/online-payment-steps.component';

@NgModule({
  declarations: [
    OnlinePaymentComponent,
    OnlinePaymentStepsComponent
  ],
  imports: [
    ShellModule,
    SharedModule,
    CtaModule,
    TestimonialsModule,
    UsualModule,
    ActionsModule,
    OnlinePaymentRoutingModule,
    SlidersModule,
  ]
})
export class OnlinePaymentModule {}
