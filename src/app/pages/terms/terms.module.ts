import { NgModule } from '@angular/core';
import { FootersModule } from 'src/app/blocks/footers/footers.module';
import { ShellModule } from 'src/app/shell/shell.module';

import { TermsRoutingModule } from './terms-routing.module';
import { TermsComponent } from './components/terms/terms.component';
import { TermsTermsComponent } from './components/terms-terms/terms-terms.component';

@NgModule({
  declarations: [TermsComponent, TermsTermsComponent],
  imports: [FootersModule, ShellModule, TermsRoutingModule]
})
export class TermsModule {}
