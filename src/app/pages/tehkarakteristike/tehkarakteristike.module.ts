import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { ShellModule } from 'src/app/shell/shell.module';
import { SlidersModule } from 'src/app/blocks/sliders/sliders.module';
import { AppTehkarakteristikeRoutingModule } from './app-tehkarakteristike-routing.module';
import { TehKarakteristikeComponent } from './components/teh-karakteristike/teh-karakteristike.component';

@NgModule({
  declarations: [TehKarakteristikeComponent],
  imports: [
    AppTehkarakteristikeRoutingModule,
    CommonModule,
    SharedModule,
    ShellModule,
    SlidersModule
  ]
})
export class TehkarakteristikeModule {}
