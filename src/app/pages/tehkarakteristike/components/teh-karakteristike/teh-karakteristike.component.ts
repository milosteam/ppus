import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/core/content.service';
import { faLightbulb } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LogoService } from 'src/app/shell/services/logo.service';

@Component({
  selector: 'dc-teh-karakteristike',
  templateUrl: './teh-karakteristike.component.html',
  styleUrls: ['./teh-karakteristike.component.scss']
})
export class TehKarakteristikeComponent implements OnInit, AfterViewInit {
  header: Observable<any>;

  constructor(private cnts: ContentService, private logos: LogoService) { }

  ngOnInit(): void {
    this.header = this.cnts.$tehkarakteristike.pipe(map(data => data.text));
    this.logos.setLogo(false);
  }

  ngAfterViewInit(): void {
    this.logos.setPageIsLoading(false);
  }
}
