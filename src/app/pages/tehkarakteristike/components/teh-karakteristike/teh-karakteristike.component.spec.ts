import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TehKarakteristikeComponent } from './teh-karakteristike.component';

describe('TehKarakteristikeComponent', () => {
  let component: TehKarakteristikeComponent;
  let fixture: ComponentFixture<TehKarakteristikeComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TehKarakteristikeComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TehKarakteristikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
