import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from '../../core';

import { TehKarakteristikeComponent } from './components/teh-karakteristike/teh-karakteristike.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  {
    path: '',
    component: TehKarakteristikeComponent,
    data: { title: extract('Tehnicke karakteristike') }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class AppTehkarakteristikeRoutingModule {}
