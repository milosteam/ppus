export const COOKIECONSENTEN = {
  header: 'Cookies used on the website!',
  message:
    'We use cookies on our website to give you the most relevant experience by remembering your preferences and repeat visits. By clicking “Accept”, you consent to the use of ALL the cookies.',
  dismiss: 'Prihvatam',
  allow: 'Allow cookies',
  deny: 'Decline',
  link: 'PRIVACY IS IMPORTANT...',
  href: '/en/privacy_policy'
};
