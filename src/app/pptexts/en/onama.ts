export const ONAMAEN = [
  {
    title: 'ABOUT US',
    description:
      'Our diversity arises from a proactive and innovative problem-solving approach, led by integrity, with the goal of developing cooperation with clients that have the same vision.'
  },
  {
    title: 'ABOUT PAPIR PRINT',
    texts: [
      'Papir Print  (LLC) is a limited liability company, which has been successfully operating on the domestic and foreign markets for 26 years, with the main  focus on graphic activity - printing.' +
      +'From its beginnings, the company has been recording continuous growth of business capacities, which is directly reflected in the growth of production volume from year to year. The production range of the company is intended mainly for the food, chemical and pharmaceutical industries. However, flexibility and cooperative relations enable the production of packaging for all other industrial branches according to the customer specification.',
      'Papir Prints fulfills the high criteria of ISO:9001: 2015, ISO:14001:2015 i ISO 22000 ( HACCP) standards, as well as the requirements of modern business for short delivery terms of high quality products and safe packaging of products that are most sensitive.',
      'The company monitors and implements all technological achievements in the field of flexo-printing. The result of modern production and long-term experience has been confirmed by several awards (the one won in London for Flexo Print and the Award for the best print in the narrow web category under the auspices of Flexo Tech International).'
    ],
    img: {
      path: 'assets/img/papirprint/sr/kompanija/kompanija-papirprint',
      alt: '',
      label: ''
    }
  },
  {
    title: 'MISSION AND VISION',
    texts: [
      'OUR MISSION - We are a result oriented printing company dedicated to developing relationships with our clients, providing expertise for the development and improvement of the value of their products, while ensuring the profitability of both our clients and our company. Papir Print, as a value-oriented organization, offers the highest quality of print using the cutting-edge technology and experience of our employees. We maintain cooperation with our clients by applying ethical standards in a socially responsible working environment.',
      'OUR VISION - Our values based on the principles will be the basis for our future development. A stimulating work position along with developed communication systems, training, exchanges and individual growth will ensure full satisfaction both of our clients and employees. Papir Print aims to become synonymous with the highest quality printing, and above all, is synonymous with quality in the creation of the latest flexible packaging, with maximum efficiency and effectiveness, with the application of international standards, both in Serbia and in all countries of Eastern and Western Europe.'
    ],
    img: {
      path: 'assets/img/papirprint/sr/kompanija/misija-vizija',
      alt: '',
      label: ''
    }
  },
  {
    title: 'SOCIAL RESPONSIBILITY',
    texts: [
      'RESPONSIBILITY* The employees of Papir Print keep their promises, fulfill their obligations and they are committed to the success and well-being of the team.',
      'LOYALTY* We set loyalty at the heart of our business.',
      'CORPORATE SOCIAL RESPONSIBILITY* The employees in Papir Print perform their work in accordance with the highest international ethical standards, respecting people and the environment. For many years, we are part of the global SEDEX platform.',
      'PARTNERSHIP* We highly value and respect employees and their commitment to business, because they represent the pillars of our company. Recruiting, training and keeping dedicated people are some of the main goals of Papir Print.',
      'INNOVATION* Innovation in all fields is crucial to the development of our business. It is a priority of our organization in the development of new products and processes that improve production capacities.',
      'ENERGY AND ECOLOGY* Our constant goal is recycling and waste minimization, the use of secondary raw materials and energy efficiency optimization. This statement is strongly supported by the certified ISO 14001 standard, and the implementation of the ISO 50001 standard is in progress.'
    ],
    img: {
      path:
        'assets/img/papirprint/sr/kompanija/drustvena-odgovornost',
      alt: '',
      label: ''
    }
  }
];
