export const SERTIFIKATIEN = [
  {
    icon: 'star',
    title: 'SRPS EN ISO 22000 : 2018',
    description: 'FOOD SAFETY MANAGEMENT SYSTEMS'
  },
  {
    icon: 'star',
    title: 'SRPS ISO 9001 : 2015',
    description: 'QUALITY MANAGEMENT SYSTEM'
  },
  {
    icon: 'star',
    title: 'SRPS ISO 14001 : 2015 ',
    description: 'ENVIRONMENTAL MANAGEMENT SYSTEM '
  }
];
