const Dizajn = {
  imeMasine: 'Dizajn studio',
  slikaMasine: '/heliostar',
  spec: [
    '10 tehničara',
    'Adobe Studio',
    'Server Bakro',
    'Server Flekso',
    'Štampač za pruf'
  ]
};

const Heliostar = {
  imeMasine: 'Heliostar',
  slikaMasine: '/heliostar',
  spec: [
    'Prints up to 9 colors.',
    'Maximum print width 1000 mm.',
    'Maximum step 900 mm.',
    'Maximum speed 300 m/min.',
    'Automatic registry control.',
    'Cold seal application inline and offline.',
    'Possibility of a reverse use of varnishes in the registry.',
    'Double-sided printing.'
  ]
};

const Miraflex = {
  imeMasine: 'Miraflex',
  slikaMasine: '/miraflex',
  spec: [
    'Prints up to 8 colors.',
    'Maximum print width 1270 mm.',
    'Maximum step 800 mm.',
    'Maximum speed 400 m/min.'
  ]
};
const Primaflex = {
  imeMasine: 'Primaflex',
  slikaMasine: '/primaflex',
  spec: [
    'Prints up to 8 colors.',
    'Maximum print width 810 mm.',
    'Maximum step 800 mm.',
    'Maximum speed 300 m/min.'
  ]
};

const Supersimplex = {
  imeMasine: 'Super Simplex',
  slikaMasine: '/simplex',
  spec: [
    'Creating multilayered materials with and without barrier.',
    'Maximum laminating width 1300 mm.',
    'Solvent-free laminating.',
    'Automatic registry control.'
  ]
};

const Supercombi = {
  imeMasine: 'Super Combi 3000',
  slikaMasine: '/combi',
  spec: [
    'Creating multilayered materials with and without barrier.',
    'Maximum laminating width 1300 mm.',
    'Maximum laminating speed 400 m.',
    'Laminating with and without solvents.',
    'Cold seal application in the registry.'
  ]
};

const Kampf = {
  imeMasine: 'Kampf',
  slikaMasine: '/kampf',
  spec: [
    'Three lines.',
    'Minimum tape width 30 mm.',
    'Automatic changes.',
    'High precision level.',
    'High speed.'
  ]
};

const Nishibe = {
  imeMasine: 'Nišibe',
  slikaMasine: '/nishube',
  spec: [
    'Four lines.',
    'High precision level.',
    'Minimum width of the bag 40 mm.'
  ]
};

const Ekstruder = {
  imeMasine: 'Ekstruder',
  slikaMasine: '/multicool',
  spec: [
    'Automatska linija za izradu valjaka za duboku štampu.',
    'Izrada klišea za flekso štampu.',
    'Automatska priprema, doziranje i nijansiranje boja prema nalogu.'
  ]
};

const Ostalo = {
  imeMasine: 'Ostali kapaciteti',
  slikaMasine: '/multicool',
  spec: [
    'Two automatic lines for making deep-print rollers.',
    'Making flexo-printing clichés.',
    'Automatic preparation, dosing and tinting colors according to your order.'
  ]
};

export const TEHKARAKTERISTIKEEN = {
  roto: {
    opis: 'ROTO ŠTAMPA',
    oprema: [Heliostar]
  },
  flexo: {
    opis: 'FLEKSO ŠTAMPA',
    oprema: [Miraflex, Primaflex]
  },
  laminiranje: {
    opis: 'LAMINIRANJE',
    oprema: [Supercombi, Supersimplex]
  },
  secenje: {
    opis: 'SEČENJE',
    oprema: [Kampf]
  },
  konfekcioniranje: {
    opis: 'KONFEKCIONIRANJE',
    oprema: [Nishibe]
  },
  ostalo: {
    opis: 'OSTALA OPREMA',
    oprema: [Ekstruder]
  }
};

export const TEHKARAKTERISTIKEEN2 = {
  text: {
    title: 'Technical characteristics',
    text:
      'In-house print preparation, key processes secured with multiple autonomous equipment lines ' +
      'and fully equiped quality control laboratory are just some of the benefits we provide to our customers.'
  },
  oprema: [
    {
      title: 'The print',
      description: 'Test',
      data: [
        {
          roto: {
            opis: 'ROTO PRINTING HELIOSTAR',
            oprema: [Heliostar]
          },
          flexo: {
            opis: 'FLEXO PRINTING MIRAFLEX',
            oprema: [Miraflex]
          },
          flexo2: {
            opis: 'FLEXO PRINTING MIRAFLEX',
            oprema: [Primaflex]
          }
        }
      ]
    },
    {
      title: 'Lamination',
      description: 'Test',
      data: [
        {
          laminiranje: {
            opis: 'LAMINATION SUPERCOMBI',
            oprema: [Supercombi]
          },
          laminiranje2: {
            opis: 'LAMINATION SUPERSIMPLEX',
            oprema: [Supersimplex]
          }
        }
      ]
    },
    {
      title: 'Cutting and bag confectioning',
      description: 'Test',
      data: [
        {
          secenje: {
            opis: 'CUTTING',
            oprema: [Kampf]
          },
          konfekcioniranje: {
            opis: 'CONFECTIONING',
            oprema: [Nishibe]
          }
        }
      ]
    },
    {
      title: ' Other Capacities ',
      description: 'Test',
      data: [
        {
          secenje: {
            opis: 'SEČENJE',
            oprema: [Ostalo]
          }
        }
      ]
    }
  ]
};
