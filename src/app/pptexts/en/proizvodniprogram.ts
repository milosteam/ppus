export const PROIZPROGRAMEN = {
  '/en/production-program/packaging-food-industry': {
    categoryData: {
      categoryNameShort: 'Food industry',
      categoryName: 'Packaging - Food industry',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/en/productionprogram/packaging-food-industry',
          alt: 'In this image packaging for food industry is shown.',
          label: ''
        }
      ],
      categoryTexts: [
        'The food industry products are classified in the group of the most sensitive and demanding products, and therefore, their packaging must ensure the minimal influence of external factors with maximum quality preservation.',
        'In addition to its high hygienic and chemical-technological properties, modern flexible packaging must respond to the increasingly demanding print quality in HD + resolution, as well as to provide the product with the most sophisticated designs to attract potential customers.'
      ]
    },
    categoryProducts: {
      'packaging-snacks': {
        productName: 'Chips, flips and other snacks',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-chips-flips-snacks01',
          alt:
            'In this image packaging for chips, flips and other snacks is shown.',
          label: ''
        },
        productTexts: [
          'We produce packaging suitable for a group of widely-sold consumer goods, characterized by the most diverse range of smells, aromas and flavours. Preserving this sophisticated taste and freshness requires flexible packaging, consisting of single and multi-layer barrier films. It often requires double-sided printing and high HD + quality of flexo printing. When it comes to premium products, many customers opt for a deep print of prestigious quality.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-chips-flips-snacks02',
              alt:
                'In this image polyethylene film with extrusion machine is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-confectionery': {
        productName: 'Confectionery',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-confectionery01',
          alt: 'In this image confectionery packaging is shown.',
          label: ''
        },
        productTexts: [
          'Sweet dairy products, tastefully enriched with a wide variety of films, especially sensitive to external factors and temperature variations, require special packaging, which must be attractive, striking, with single and multi-layer barrier films. Standard HD + quality is applied, and a super-light film is added for a prestigious appearance. The possibility of forming a package with a hot and cold seal is a feature of this packaging.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-confectionery02',
              alt: 'In this image confectionery packaging is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-biscuits-cookies': {
        productName: 'Biscuits and cookies',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-biscuits-cookies01',
          alt: 'In this image packaging for biscuits and cookies is shown.',
          label: ''
        },
        productTexts: [
          'The flexible packaging of premium quality and high resolution, specially designed for quick-packing and packet-shaped packages with both horizontal and vertical seal (for VFFS and HFFS packers) is suitable for biscuits and cookies. The choice of appropriate barrier materials ensures adequate protection and durability of these products.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-biscuits-cookies02',
              alt: 'In this image packaging for biscuits and cookies is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-coffee-drinks': {
        productName: 'Coffee and hot drinks',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-coffee-drinks01',
          alt: 'In this image packaging for coffee and hot drinks is shown.',
          label: ''
        },
        productTexts: [
          'Coffee in grains, ground coffee, coffee beans and instant coffee require high barriers towards moisture, oxygen and light penetration while preserving aromatic properties. Triplex high-barrier films are commonly used, with the possibility of forming a wide variety of packaging forms.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-coffee-drinks02',
              alt:
                'In this image packaging for coffee and hot drinks is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-soups-spices-aromas': {
        productName: 'Soups, spices and aromas',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-soups-spices-aromas01',
          alt: 'In this image packaging for soups, spices and aromas is shown.',
          label: ''
        },
        productTexts: [
          'Preservation of flavour, aroma and consistency of dry nuts, dehydrated foods and beverages, sauces, ready meals, spices and food additives requires the use of high barrier materials. The resolution of  HD + flexo printing is usually standard, and roto-gravure is used for premium product lines. Opaque, semi-transparent and transparent films are used to improve the aesthetic appeal of the packaging.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-soups-spices-aromas02',
              alt:
                'In this image packaging for soups, spices and aromas is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-icecream-frozenproducts': {
        productName: 'Ice cream and frozen products',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-icecream-frozenproducts01',
          alt:
            'In this image packaging for ice cream and frozen products is shown.',
          label: ''
        },
        productTexts: [
          'Packaging for this type of product must be extremely high quality and durable at low temperatures. For frozen products, low permeability of heat and light is of particular importance. HD + flexo printing with a combination of high-quality barrier materials provide excellent price and quality ratio.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-icecream-frozenproducts02',
              alt:
                'In this image packaging for ice cream and frozen products is shown.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/en/production-program/packaging-beverage': {
    categoryData: {
      categoryNameShort: 'Beverage industry',
      categoryName: 'Packaging - Beverage industry',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/en/productionprogram/packaging-beverage-industry',
          alt: 'In this image packaging beverage industry is shown.',
          label: ''
        }
      ],
      categoryTexts: [
        'Products of flexible materials have been widely used in the beverage industry as a common addition to PET packaging.',
        ' In addition to highlighting all the necessary information about the product, wrap and multipack packaging have a specific role in strengthening the brand, and customers are already used to recognizing their favourite beverages easily, due to attractive packaging.'
      ]
    },
    categoryProducts: {
      'packaging-wrap-label': {
        productName: 'Wrap label',
        productImgs: {
          path: 'assets/img/papirprint/en/products/packaging-wrap-label01',
          alt: 'In this image wrap label packaging is shown.',
          label: ''
        },
        productTexts: [
          'This is a wrap flexible packaging with HD + flexo or deep roto-gravure, attractive appearance and prestigious quality. It requires the use of specific equipment to reduce the presence of static electricity, which ensures smooth operation when forming this type of packaging.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-wrap-label02',
              alt: 'In this image wrap label packaging is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-standup-pouch': {
        productName: 'Stand up pouch',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-standup-pouch01',
          alt: 'In this image stand up pouch packaging is shown.',
          label: ''
        },
        productTexts: [
          'Stand up flexible packaging is a package of modern design, which allows the bags with a bottom gusset to stand on shelves, while the flat surface of the bag is ideal for attractive design solutions. It is suitable for packaging of various types of food, non-food, powder, dehydrated, chemical, liquid and other products. It has been a great replacement for the traditional bag-in-box, glass and metal packaging.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-standup-pouch02',
              alt: 'In this image stand up pouch packaging is shown.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/en/production-program/packaging-pharmaceutical-cosmetic': {
    categoryData: {
      categoryNameShort: 'Pharmacy and cosmetics',
      categoryName: 'Packaging -  Pharmacy and cosmetics',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/en/productionprogram/packaging-pharmaceutical-cosmetic',
          alt: 'In this image pharmacy and cosmetics packaging is shown.',
          label: ''
        }
      ],
      categoryTexts: [
        'Products of the chemical industry have a wide use in a variety of areas, and most often these are products in the form of liquid and powder of different chemical composition with aggressive and hygroscopic properties.',
        'The packaging necessary for this group of articles has to ensure adequate resistance of the packaging to the chemical composition of the product, meet the high sanitary standards and, by all means, ensure high resistance of the packaging to mechanical and external influences.'
      ]
    },
    categoryProducts: {
      'packaging-cosmetics-personalhygiene': {
        productName: 'Cosmetics and personal hygiene products',
        productImgs: {
          path:
            'assets/img/papirprint/en/products/packaging-cosmetics-personalhygiene01',
          alt:
            'In this image packaging for cosmetics and personal hygiene products is shown.',
          label: ''
        },
        productTexts: [
          'This product group requires three-layer or four-layer barrier materials, which are impermeable to air (protection against gas/aroma loss), thus preventing the weakening of chemical ingredients of the product. Packaging is characterized by high tensile/compressive strength, and it is very easy to use. It is possible to apply both flexo printing technology and roto-gravure for the premium product line.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-cosmetics-personalhygiene02',
              alt:
                'In this image packaging for cosmetics and personal hygiene products is shown.',
              label: ''
            }
          ]
        }
      },
      'packaging-supplements': {
        productName: 'Supplements',
        productImgs: {
          path: 'assets/img/papirprint/en/products/packaging-supplements01',
          alt: 'In this image packaging for supplement products is shown.',
          label: ''
        },
        productTexts: [
          'Flexible packaging, known as a blister pack (a cavity or pocket formed inside the packaging) that protects the product from external factors such as humidity and contamination for an extended time period. Opaque blisters also protect sensitive products from daylight. This packaging is commonly used as a unit-dose packaging for pharmaceutical tablets, capsules or lozenges, providing protection in product storage (store shelves).'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/en/products/packaging-supplements02',
              alt: 'In this image packaging for supplement products is shown.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/en/production-program/production-pe-films': {
    categoryData: {
      categoryNameShort: 'Polyethylene film',
      categoryName: 'Packaging - Production of polyethylene film',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/en/productionprogram/polyethylene-film',
          alt: 'In this image polyethylene film is shown.',
          label: ''
        }
      ],
      categoryTexts: [
        'A wide range of packaging products is completed by producing polyethylene films for their own needs and for the needs of customers. Paper Print produces LDPE films for various purposes in a thickness range from 25 μm to 120 μm.',
        'Polyethylene packaging protects the product and extends its lifetime, with the possibility of a variety of design and printing. Packaging made of polyethylene must satisfy extremely stringent requirements of health safety, so packaging control is carried out continuously in the relevant authorized institutions.'
      ]
    },
    categoryProducts: {
      'proizvodnja-polietilen': {
        productName: ' Production of polyethylene film',
        productImgs: {
          path: 'assets/img/papirprint/en/products/polyethylene-film',
          alt:
            'In this image polyethylene film with extrusion machine is shown.',
          label: ''
        },
        productTexts: [
          'Following the trends and the market, significant improvement of technical and technological procedures in production was achieved by introducing a line for the co-extrusion of blown polyethylene film. ',
          'Since 2014, our company has had a three-layer coextruder of the famous brand Windmoller & Holcher. The basic characteristics are reflected in the possibilities of making polyethylene thickness ranging from 20 to 200μm, as well as from 800 to 1800mm and a maximum capacity of 870 kg/h. ',
          'Since our team works on expanding their knowledge and perfecting skills, Papir Print is capable of meeting the needs of its production, and responding to the requests of other customers. ' +
            'The quality of polyethylene production is confirmed in cooperation with the laboratory. '
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path: 'assets/img/papirprint/en/products/polyethylene-film',
              alt:
                'In this image polyethylene film with extrusion machine is shown.',
              label: ''
            }
          ]
        }
      }
    }
  }
};
