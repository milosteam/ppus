export const CONTACTEN = {
  header: {
    title: 'Contact info',
    text:
      'Thank you for visiting our Web Portal and for your interest in our services. You can contact us by E-mail, web site, phone or visit in person. All contact information can be found on this page.'
  },
  data: [{}, {}]
};
