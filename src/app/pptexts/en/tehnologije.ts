export const TEHNOLOGIJEPOCETNAEN = [
  {
    icon: 'activity',
    title: 'DIGITAL BUSINESS',
    description:
      'From order to delivery, using the latest ERP / MIS software solution for the printing industry.'
  },
  {
    icon: 'file',
    title: 'PRE-PRESS AND PRINTING FORM',
    description:
      'In-house prepress, especially configured for our equipment, regardless of whether it is a roto or flexo printing.'
  },
  {
    icon: 'settings',
    title: 'INDUSTRY 4.0',
    description:
      'Predefined production in the hands of experienced operators, using the latest autonomous printing and logging systems.'
  },
  {
    icon: 'lock',
    title: 'QUALITY CONTROL',
    description:
      'A fully equipped laboratory led by a team of experienced chemical engineers controls the production process from the receipt of materials to the delivery of the finished product.'
  }
];
