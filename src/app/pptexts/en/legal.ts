export const LEGALEN = {
  headerText: [
    'Privacy Policy',
    'It is often necessary to store or download a minimum amount of information from your device in order to provide an exceptional user experience. According to the latest legislation, due to the sensitivity of the data that might be used, it is necessary to obtain the consent of the user before using such tools.'
  ],
  cookieText: [
    'Cookies',
    'Cookies are very small text files on your computer set by a Web server when you visit a site. Modern websites use them to provide various functionalities.'
  ],
  shortTexts: [
    {
      question: 'Storage settings',
      answer:
        'When you visit our websites, after accepting the * cookie policy (advance tools), we store or download data from your internet browser.'
    },
    {
      question: 'Personalization settings',
      answer:
        'These functionalities allow our internet application to remember your decisions (language or region) and improve your user experience and content quality.'
    },
    {
      question: 'Analytics',
      answer:
        'These settings allow us to monitor the content that users visit the most and improve our website based on the data obtained.'
    },
    {
      question: 'Security settings',
      answer:
        'User verification is performed using various tools to protect both the user and the application itself.'
    }
  ],
  longTexts: [
    // 0
    {
      title: 'Privacy Policy - PAPIR PRINT',
      text: ['The privacy policy of Papir Print aims to clearly explain how websites www.papirprint.co and www.papirprint.rs collect and protect your data and therefore take care of your privacy.']
    },
    // 1
    {
      title: 'What information we collect from site visitors',
      text: ['*Our websites, without your permission do not use advanced technologies that perform data collection or storage. As a result of this, some functionalities will not be available to you before accepting cookie consent. If you accept the use of cookies, depending on the purpose, we will collect or remember certain data or parameters. You can change your consent decision at any time by activating the cookie bar in the lower-left corner of the page. ',
      'The list of services that we use:']
    },
    // 2
    {
      title: 'Google analytics',
      text: ['is Google Inc. service, which we use to analyze the quality of service, search analytics, frequency of visits to specific content, etc. Information about your activities is recorded in a cookie and then collected and stored on Google servers (physically located in the USA). Analytics is used with the "_anonymizeIP ()" option, which means that Google will shorten your IP address so that it is not possible to detect your exact location but only a wider geographical position. Google will not analyze the specified IP address with other data in its possession. Based on the collected data, Google will create reports on behalf of the owner of Internet content on the most visited pages, the quality of service, the number of users, and such statistics are necessary for us to improve the quality of Internet content.']
    },
    // 3
    {
      title: 'Google ReCAPTCHA v2',
      text: ['is a security service of Google Inc. which uses cookies and collects your data. The main role of this functionality is to improve the quality of the service by checking whether the user is a human or a program. Through its algorithms, based on the data it collects, such as IP address, the number of mouse clicks, correct answers to human only questions, Google ReCAPTCHA analyzes user activity, and protects web application.']
    },
    // 4
    {
      title: 'We use geolocation detection',
      text: ['to localize the webpage content. We do not store or use this information for any other purpose.']
    },
    // 5
    {
      title: 'Collection of personal information',
      text: ["*Personal information is information about a visitor that precisely identifies the user's identity, such as name and surname, e-mail address, or telephone number. Visitors can only provide this type of information voluntarily through specific inquiries such as contact forms, job applications, or requests for quotations."]

    },
    // 6
    {
      title: 'Security of collected information and data retention',
      text: [
        'Papir Print Internet applications exclusively use the HTTPS protocol when interacting with the user. HTTPS simply encrypts data between the user and the website itself - from the data that the user sends to those that are received from the other party. Encrypted data, even if it comes into the possession of a third party, is secure - it is very difficult or completely impossible to decrypt.',
        'The information that visitors share with us is stored on servers in our company and can only be accessed by authorized persons employed by the company and other related persons.'+ 
        'Paper Print applies strict physical, electronic, and administrative safeguards to protect visitor information from unauthorized access and  unlawful processing, accidental loss, destruction, or damage, both online and offline. We will keep information about you within the time limit required by law or within a reasonable time in case the law does not regulate it. To prevent unauthorized access or disclosure, maintain the accuracy of the data and ensure the proper use of personal data, Papir Print has adopted appropriate physical, electronic and administrative procedures to protect and secure the personal data it processes.'
      ]
    },
    // 7
    {
      title: 'Consent for certain categories of visitors',
      text: ['*If the visitor is less than 16 years of age, it is necessary to obtain the consent of the parent or legal representative before sending any personal information to the company.']
    },
    // 8
    {
      title: 'Internet communication and disclaimer',
      text: ["*Given that the Internet is a global environment, the use of a website to collect and process personal information necessarily involves the transmission of data around the world. Therefore, by viewing this website and electronic communication with us, visitors confirm and agree that we also process personal information in this way. Although Papir Print implements measures to protect against viruses and other dangerous components, the nature of the Internet is such that it is not possible to ensure that a visitor's access to the website will be uninterrupted or error-free."]
    },
    // 9
    {
      title: 'Information access',
      text: ['*Visitors have a legal right to view personal information that the company has about them and may request that we make the necessary changes to ensure their accuracy and up-to-dateness. If they wish, visitors have the right to contact us via the contact information on the Contact page. The company has the right to charge the visitor the necessary administrative costs of providing information, or acting on the request, under the law.']
    },
    //  10
    {
      title: 'Contact',
      text: ['*If the visitor has any comments, questions or needs to obtain information related to the Privacy Policy, our visitor can contact us via the information in the Contact section.']
    },
    //  11
    {
      title: 'Policy update',
      text: ['*Paper Print reserves the right to change this Policy. We may amend or update portions of this privacy statement without notice to you in advance. Always check the data privacy statement before using our site to be informed of the latest status in case of any changes or updates. By using any content on the site, it is considered that the visitor is familiar with the latest rules on terms of use and privacy policy.']
    }

  ]

};