export const LEGALSR = {
  headerText: [
    'Politika privatnosti',
    'U cilju poboljšanja korisničkog iskustva kao i besprekornog funkcionisanja internet stranice, često je potrebno minimalnu količinu informacija čuvati ili preuzimati sa vašeg uređaja. Prema najnovijoj zakonskoj regulativi, zbog osetljivosti podataka koji se mogu koristiti, pre upotrebe ovakvih alata neophodno je dobiti saglasnost korisnika.'
  ],
  cookieText: [
    'Kolačići (Cookies)',
    'Kolačići (Cookies) su vrlo male tekstualne datoteke u vašem računaru koje postavlja Web server kada posetite neki sajt. Moderne internet stranice ih koriste da obezbede različite funkcionalnosti.'
  ],
  shortTexts: [
    {
      question: 'Postavke skladištenja',
      answer:
        'Kada posetite naše internet lokacije, nakon prihvatanja *cookie policy (naprednih alata), mi čuvamo ili preuzimamo podatke iz vašeg internet pretraživača.'
    },
    {
      question: 'Postavke personalizacije',
      answer:
        'Ove funkcionalnosti omogućavaju da naša internet aplikacija pamti vaše odluke (jezik ili region), i na osnovu njih unapređuje vaše korisničko iskustvo i prilagođava kvalitet sadržaja.'
    },
    {
      question: 'Analitika',
      answer:
        'Ove postavke omogućavaju nam da pratimo sadržaj koji korisnici najčešće posećuju i da na osnovu dobijenih podataka adekvatno unapređujemo našu internet stranicu.'
    },
    {
      question: 'Bezbednosne postavke',
      answer:
        'Korišćenjem različitih alata vršimo proveru korisnikovih unosa u cilju zaštite kako korisnika tako i same aplikacije.'
    }
  ],
  longTexts: [
    // 0
    {
      title: 'Politika privatnosti - PAPIR PRINT',
      text: ['Politika privatnosti kompanije Papir Print ima za cilj da svim posetiocima internet stranica www.papirprint.co i www.papirprint.rs, a u cilju zaštite njihove privatnosti, jasno obrazloži kako i koje podatke prikuplja, kako ih koristi i kako brine o njihovoj bezbednosti.']
    },
    // 1
    {
      title: 'Koje informacije prikupljamo od posetilaca sajta',
      text: ['*Naše internet lokacije, bez vaše dozvole ne koriste napredne tehnologije koje vrše prikupljane ili čuvanje podataka. Vaše mogućnosti su ograničene a neke funkcionalnosti neće vam biti na raspolaganju. Ukoliko prihvatite korišćenje ovih tehnologija aktiviraju se alati koji koriste tzv. kolačiće koji u zavisnosti od namene prikupljaju ili pamte određene podatke ili parametre. U svakom trenutku možete da promenite odluku o saglasnosti, aktiviranjem trake kolačića u donjem levom uglu internet stranica.',
        'Od naprednih usluga naše internet lokacije koristi tzv. alate trećih lica koji kreiraju i koriste kolačiće a to su:']
    },
    // 2
    {
      title: 'Google analytics',
      text: ['proizvod kompanije Google Inc, koji koristimo za analizu kvaliteta usluge, analitiku pretrage, frekfentnosti posete određenom sadržaju itd. Informacije o vašim aktivnostima se beleže u kolačić a zaztim se prikupljaju na Google servere i tamo čuvaju (fizički locirani u USA). Analitika se koristi sa opcijom „_anonymizeIP()“, što znači da će Google vašu IP-adresu skratiti tako da nije moguće detektovati vašu tačnu lokaciju već samo širu geografsku poziciju. Navedenu IP-adresa Google neće analizirati sa ostalim podacima koje poseduje. Na osnovu prikupljenih podataka kompanija Google će u ime vlasniku Internet sadržaja kreirati izveštaje o najposećenijim stranicama, kvalitetu usluge, broju korisnika a takva statistika je nama neophodna da bismo unapredili kvalitet internet sadržaja.']
    },
    // 3
    {
      title: 'Google ReCAPTCHA v2,',
      text: ['takođe proizvod komapnije Google Inc iz domena bezbednosnih mera, koji za svoje funkcionisanje koristi kolačiće i prikuplja Vaše podatke. Glavna uloga ovog dodatka je da spreči ugrožavanje kvaliteta usluge internet stranice tako što proverava da li je korisnik čovek ili maliciozni program. Google ReCAPTCHA kroz svoje algoritme analizira korisnikovu aktivnost na osnovu podataka koje prikuplja a to su: IP adresa, broj klikova mišem, provera postojanja drugih kolačića kao i postavljanjem pitanja na koje programi ne mogu dati tačne odgovore i na taj način štiti i korisnika a i aplikaciju od eventualne zloupotrebe.']
    },
    // 4
    {
      title: 'Koristimo detekciju geolokacije na osnovu IP adrese,',
      text: ['u cilju lokalizacije sadržaja ali to radimo samo prilikom pokretanja internet stranice i istu ne čuvamo niti koristimo u bilo kakve druge svrhe.']
    },
    // 5
    {
      title: 'Prikupljanje ličnih informacija',
      text: ['*Lične informacije su informacije o posetiocu na osnovu kojih ga je moguće precizno identifikovati, kao što su ime i prezime, e-mail adresa ili broj telefona. Ovu vrstu informacija posetioci mogu jedino da nam dobrovoljno proslede kroz specifčne upite kao što su kontak forma, aplikacije za posao ili zahtevi za ponudu.']

    },
    // 6
    {
      title: 'Bezbednost prikupljenih informacija i zadržavanje podataka',
      text: [
        'Internet aplikacije kompanije Papir Print isključivo koriste HTTPS protkol prilikom interakcije sa korisnikom. HTTPS najprostije rečeno enkriptuje podatke između korisnika i samog internet sajta — od onih podataka koje korisnik šalje do onih koje prima sa druge strane. Enkriptovani podaci su, čak iako dođu u posed nekog trećeg lica, sigurni — jako ih je teško ili potpuno nemoguće dekriptovati.',
        'Informacije koje posetioci podele sa nama, skladište se na serverima u našoj kompaniji, i njima mogu pristupati samo ovlašćena lica koja su zaposlena u kompaniji i druga povezana lica. Papir Print primenjuje stroge mere fizičke, elektronske i administrativne zaštite u cilju zaštite informacije o posetiocima od pristupa neovlašcenih osoba i od nezakonite obrade, slučajnog gubitka, uništenja ili oštećenja kako onlajn tako i oflajn. Mi ćemo čuvati informacije o Vama u roku kako to zakon nalaže ili u razumnom roku u slučaju da to zakon ne reguliše. U cilju sprečavanja neovlašcenog pristupa ili otkrivanja, održavanja tačnosti podataka i obezbedivanja odgovarajuceg korišcenja ličnih podataka, Papir Print je usvojio odgovarajuće fizičke, elektronske i upravne procedure kako bi zaštitio i obezbedio lične podatke koje obrađuje.'
      ]
    },
    // 7
    {
      title: 'Saglasnost za određene kategorije posetilaca',
      text: ['*Ukoliko posetilac ima manje od 16 godina života potrebno je pre nego što pošalje kompaniji bilo koje lične informacije da pribavi saglasnost roditelja, odnosno zakonskog zastupnika. Posetiocima bez navedene saglasnosti nije dozvoljeno da šalju kompaniji informacije.']
    },
    // 8
    {
      title: 'Prenos putem interneta i odricanje',
      text: ['*S obzirom na to da internet predstavlja globalno okruženje, korišćenje internet stranice za prikupljanje i obradu ličnih informacija nužno obuhvata i prenos podataka širom sveta. Stoga pregledanjem ove internet stranice i elektronskom komunikacijom sa nama posetioci potvrđuju i saglasni su da i mi obrađujemo lične informacije na taj način. Mada Papir Print sprovodi mere zaštite od virusa i drugih opasnih komponenata, priroda interneta je takva da nije moguće obezbediti da će pristup posetioca internet stranici biti bez prekida ili grešaka, ili da ova internet stranica, njeni serveri ili elektronska pošta koju možemo poslati ne sadrže viruse ili druge opasne komponente.']
    },
    // 9
    {
      title: 'Pravo posetioca na pristup informacijama',
      text: ['*Posetioci imaju zakonsko pravo da pogledaju lične informacije koje kompanija poseduje o njima i mogu tražiti da izvršimo potrebne izmene da bismo obezbedili njihovu tačnost i ažuriranost. Ukoliko to žele, posetioci imaju pravo da nas kontaktiraju preko kontakt podataka sa stranice Kontakt. Kompanija ima pravo da posetiocu naplati nužne administrativne troškove pružanja informacije, odnosno postupanja po zahtevu, u skladu sa zakonom.']
    },
    //  10
    {
      title: 'Kontakt',
      text: ['*Ukoliko posetilac ima bilo kakvih komentara, pitanja ili potrebe za dobijanjem informacija koji se odnose na Politiku privatnosti, posetilac naš može kontaktirati putem podataka u odeljku Kontakt.']
    },
    //  11
    {
      title: 'Ažuriranje i dopuna',
      text: ['*Papir Print zadržava pravo da promeni ovu Politiku. Možemo da izmenimo ili ažuriramo delove izjave o privatnosti podataka a da vas ne obavestimo o tome unapred. Uvek proverite izjavu o privatnosti podataka pre nego što koristite naš sajt kako biste bili informisani o najnovijem statusu u slučaju bilo kakvih izmena ili ažuriranja. Korišcenjem bilo kog sadržaja na sajtu smatra se da je posetilac upoznat sa najnovijim pravilima o uslovima korišcenja.']
    }

  ]

};
