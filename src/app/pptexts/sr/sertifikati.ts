export const SERTIFIKATISR = [
  {
    icon: 'star',
    title: 'SRPS ISO 22000 : 2018',
    description: ' SISTEM MENADŽMENTA BEZBEDNOŠĆU HRANE'
  },
  {
    icon: 'star',
    title: 'SRPS ISO 9001 : 2015',
    description: 'SISTEM MENADŽMENTA KVALITETOM'
  },
  {
    icon: 'star',
    title: 'SRPS ISO 14001 : 2015',
    description: 'SISTEM UPRAVLJANJA ZAŠTITOM ŽIVOTNE SREDINE'
  }
];
