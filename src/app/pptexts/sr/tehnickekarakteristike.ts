const Dizajn = {
  imeMasine: 'Dizajn studio',
  slikaMasine: '/heliostar',
  spec: [
    '10 tehničara',
    'Adobe Studio',
    'Server Bakro',
    'Server Flekso',
    'Štampač za pruf'
  ]
};

const Heliostar = {
  imeMasine: 'Heliostar',
  slikaMasine: '/heliostar',
  spec: [
    'Štampa do 9 boja.',
    'Maksimalna širina štampe 1000 mm.',
    'Maksimalni korak 900 mm.',
    'Maksimalna brzina 300 m/min.',
    'Automatska kontrola registra.',
    'Cold seal aplikacija "inline i offline".',
    'Mogućnost reversnog nanosa lakova u registru.',
    'Obostrana štampa.'
  ]
};

const Miraflex = {
  imeMasine: 'Miraflex',
  slikaMasine: '/miraflex',
  spec: [
    'Štampa do 8 boja.',
    'Maksimalna širina štampe 1270 mm.',
    'Maksimalni korak 800 mm.',
    'Maksimalna brzina 400 m/min.'
  ]
};
const Primaflex = {
  imeMasine: 'Primaflex',
  slikaMasine: '/primaflex',
  spec: [
    'Štampa do 8 boja.',
    'Maksimalna širina štampe 810 mm.',
    'Maksimalni korak 800 mm.',
    'Maksimalna brzina 300 m/min.'
  ]
};

const Supersimplex = {
  imeMasine: 'Super Simplex',
  slikaMasine: '/simplex',
  spec: [
    'Izrada višeslojnih materijala sa i bez barijere.',
    'Maksimalna širina laminiranja 1300 mm.',
    'Laminiranje bez rastvarača.',
    'Automatka kontrola registra.'
  ]
};

const Supercombi = {
  imeMasine: 'Super Combi 3000',
  slikaMasine: '/combi',
  spec: [
    'Izrada višeslojnih materijala sa i bez barijere.',
    'Maksimalna širina laminiranja 1300 mm.',
    'Maksimalna brzina laminiranja 400 m.',
    'Laminiranje lepkovima sa i bez rastvarača.',
    'Cold seal aplikacija u registru.'
  ]
};

const Kampf = {
  imeMasine: 'Kampf',
  slikaMasine: '/kampf',
  spec: [
    'Četiri linije.',
    'Minimalna širina trake 30 mm.',
    'Automatske izmene.',
    'Visok nivo preciznosti.',
    'Velika brzina.'
  ]
};

const Nishibe = {
  imeMasine: 'Nišibe',
  slikaMasine: '/nishube',
  spec: [
    'Četiri linije.',
    'Visok nivo preciznosti.',
    'Minimalna širina kese 40 mm.'
  ]
};

const Ekstruder = {
  imeMasine: 'Ekstruder',
  slikaMasine: '/multicool',
  spec: [
    'Automatska linija za izradu valjaka za duboku štampu.',
    'Izrada klišea za flekso štampu.',
    'Automatska priprema, doziranje i nijansiranje boja prema nalogu.'
  ]
};

const Ostalo = {
  imeMasine: 'Ostali kapaciteti',
  slikaMasine: '/multicool',
  spec: [
    'Dve automatske linije za izradu valjaka za duboku štampu.',
    'Izrada klišea za flekso štampu.',
    'Automatska priprema, doziranje i nijansiranje boja prema nalogu.'
  ]
};

export const TEHKARAKTERISTIKESR = {
  roto: {
    opis: 'ROTO ŠTAMPA',
    oprema: [Heliostar]
  },
  flexo: {
    opis: 'FLEKSO ŠTAMPA',
    oprema: [Miraflex, Primaflex]
  },
  laminiranje: {
    opis: 'LAMINIRANJE',
    oprema: [Supercombi, Supersimplex]
  },
  secenje: {
    opis: 'SEČENJE',
    oprema: [Kampf]
  },
  konfekcioniranje: {
    opis: 'KONFEKCIONIRANJE',
    oprema: [Nishibe]
  },
  ostalo: {
    opis: 'OSTALA OPREMA',
    oprema: [Ekstruder]
  }
};

export const TEHKARAKTERISTIKESR2 = {
  text: {
    title: 'Tehničke karakteristike',
    text:
      'Najsavremenija oprema, ključni procesi obezbeđeni sa više autonomnih proizvodnih linija, in-house priprema i kontrola proizvodnje, garantuju sigurnu isporuku i visok kvalitet naših proizvoda.'
  },
  oprema: [
    {
      title: 'Štampa',
      description: 'Test',
      data: [
        {
          roto: {
            opis: 'ROTO ŠTAMPA HELIOSTAR',
            oprema: [Heliostar]
          },
          flexo: {
            opis: 'FLEKSO ŠTAMPA MIRAFLEX',
            oprema: [Miraflex]
          },
          flexo2: {
            opis: 'FLEKSO ŠTAMPA PRIMAFLEX',
            oprema: [Primaflex]
          }
        }
      ]
    },
    {
      title: 'Laminiranje',
      description: 'Test',
      data: [
        {
          laminiranje: {
            opis: 'LAMINIRANJE SUPERCOMBI',
            oprema: [Supercombi]
          },
          laminiranje2: {
            opis: 'LAMINIRANJE SUPERSIMPLEX',
            oprema: [Supersimplex]
          }
        }
      ]
    },
    {
      title: 'Sečenje i konfekcioniranje',
      description: 'Test',
      data: [
        {
          secenje: {
            opis: 'SEČENJE',
            oprema: [Kampf]
          },
          konfekcioniranje: {
            opis: 'KONFEKCIONIRANJE',
            oprema: [Nishibe]
          }
        }
      ]
    },
    {
      title: 'Ostali kapaciteti',
      description: 'Test',
      data: [
        {
          secenje: {
            opis: 'SEČENJE',
            oprema: [Ostalo]
          }
        }
      ]
    }
  ]
};
