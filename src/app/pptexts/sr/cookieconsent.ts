export const COOKIECONSENTSR = {
  header: 'Cookies used on the website!',
  message:
    'Poštovani, ova internet stranica koristi kolačiće i napredne funkcionalnosti za pružanje boljeg korisničkog iskustva.',
  dismiss: 'Prihvatam',
  allow: 'Prihvatam',
  deny: 'Ne prihvatam',
  link: 'Politika privatnosti...',
  href: '/sr/politika_privatnosti'
};
