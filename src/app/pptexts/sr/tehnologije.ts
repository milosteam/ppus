export const TEHNOLOGIJEPOCETNASR = [
  {
    icon: 'activity',
    title: 'DIGITALNO POSLOVANJE',
    description:
      'Od porudžbine do isporuke, korišćenjem najsavremenijeg ERP / MIS informacionog sistema za štamparsku industriju.'
  },
  {
    icon: 'file',
    title: 'PRE-PRESS I IZRADA FORME',
    description:
      'Prepress prilagođen isključivo prema opremi za izradu štamparske forme koju posedujemo, bez obzira da li je reč o dubokoj ili flekso štampi.'
  },
  {
    icon: 'settings',
    title: 'PROZVODNJA 4.0',
    description:
      'Predefinisana proizvodnja u rukama iskusnih operatera, korišćenjem najsavremenijih autonomnih štamparskih sistema.'
  },
  {
    icon: 'lock',
    title: 'KONTROLA KVALITETA',
    description:
      'Kompletno opremljena laboratorija predvođena timom tehnologa kontroliše proizvodni preces od prijema materijala do isporuke gotovog proizvoda.'
  }
];
