export const PROIZPROGRAMSR = {
  '/sr/proizvodni-program/ambalaza-prehrambena-industrija': {
    categoryData: {
      categoryNameShort: 'Prehrambena industrija',
      categoryName: 'Fleksibilna ambalaža - Prehrambena industrija',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/sr/proizvodniprogram/ambalaza-prehrambena-industrija',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za prehrambenu industriju.',
          label: ''
        }
      ],
      categoryTexts: [
        'Proizvodi prehrambene industrije svrstavaju se u grupu najosetljivijih i najzahtevnijih proizvoda, a samim tim ambalaža za njihovo pakovanje mora da obezbedi minimalni uticaj spoljnih faktora uz maksimalno očuvanje kvaliteta.',
        'Pored visokih higijenskih i hemijsko-tehnoloških osobina savremena fleksibilna ambalaža mora da odgovori i na sve zahtevniji kvalitet štampe u HD+ rezoluciji, kao i da kvalitetnim dizajnom najslikovitije dočara proizvod potencijalnim kupcima.'
      ]
    },
    categoryProducts: {
      'ambalaza-za-grickalice': {
        productName: 'ČIPS, SMOKI I OSTALE GRICKALICE',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambala-za-cips-smoki-i-ostalegrickalice01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za čips, smoki i ostale grickalice.',
          label: ''
        },
        productTexts: [
          'Proizvodimo ambalažu pogodnu za grupu visoko konzumentskih proizvoda, koju odlikuje najraznovrsniji spektar mirisa, ukusa i aroma. Očuvanje ovakvog prefinjenog ukusa i svežine zahteva fleksibilnu ambalažu, koja se sastoji od jednoslojnih i višeslojnih barijernih filmova. Neretko zahteva obostranu štampu, a visoki HD+ kvalitet flekso-štampe se podrazumeva. Kada su u pitanju premijum prozvodi, mnogi se odlučuju za  duboku štampu prestižnog kvaliteta otiska.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambala-za-cips-smoki-i-ostalegrickalice02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja za čips, smoki i ostale grickalice.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-konditorski-proizvodi': {
        productName: 'KONDITORSKI PROIZVODI',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-konditorski-proizvodi01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za konditorske proizvode, čokoladice.',
          label: ''
        },
        productTexts: [
          'Slatki mlečni proizvodi, ukusno obogaćeni najrazličitijim filovima, naročito osetljivi na spoljne faktore i temperaturne varijacije zahtevaju posebnu ambalažu, koja mora biti atraktivna, upadljiva, sa jednoslojnim i višeslojnim barijernim filmovima. Standardno se primenjuje štampa HD+ kvaliteta, a za prestižan izgled dodaje se površinski lak film. Mogućnost formiranja pakovanja uz pomoć toplog i hladnog varenog spoja odlika je ove ambalaže.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-konditorski-proizvodi02',
              alt: '',
              label: ''
            }
          ]
        }
      },
      'ambalaza-biskviti-keks': {
        productName: 'BISKVITI I KEKS',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-biskviti-i-keks01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za keks i biskvite.',
          label: ''
        },
        productTexts: [
          'Fleksibilna ambalaža premijum kvaliteta i visoke rezolucije, specijalno namenjena režimima brzog pakovanja i formiranja paketa kako horizontalnog, tako i vertikalnog spoja (za VFFS i HFFS pakerice) pogodna je za biskvite i keks. Izborom odgovarajućih barijernih materijala obezbeđuje se adekvatna zaštita i dugotrajnost ovih proizvoda.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-biskviti-i-keks02',
              alt: '',
              label: ''
            }
          ]
        }
      },
      'ambalaza-kafa-napici': {
        productName: 'KAFE I TIPLI NAPICI',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-kafe-i-toplinapici01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za kafe i tople napitke.',
          label: ''
        },
        productTexts: [
          'Kafa u zrnu, mlevena kafa, mahune za kafu i instant kafe zahtevaju ambalažu visoko barijernih karakteristika, otpornih na vlagu, prodor kiseonika i svetlosti, uz očuvanje aromatičnih svojstava. Obično se koriste tripleks barijerni filmovi, uz mogućnost formiranja najrazličitijih oblika pakovanja.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-kafe-i-toplinapici02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja za kafe i tople napitke.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-supa-zacini-arome': {
        productName: 'SUPE, ZAČINI I AROME',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-supe-zacini-i-arome01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za supe, začine i arome.',
          label: ''
        },
        productTexts: [
          'Zaštita ukusa, arome i konsistencije orašastih plodova, dehidriranoj hrani i pićima, sosevima, pripremljenim obrocima, začinima i aditivima zahteva korišćenje materijala visokih barijernih osobina. Rezolucija HD+ flekso-štampe obično je standard, a roto-gravura se koristi za premijum linije proizvoda. Neprozirni, poluprozirni i transparentni filmovi se koriste za poboljšanje estetske privlačnosti pakovanja.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-supe-zacini-i-arome02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja za supe, začine i arome.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-sladoledi-smrznuti-proizvodi': {
        productName: 'SLADOLEDI I SMRZNUTI PROIZVODI',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-sladoledi-i-smrznuti-proizvodi01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za sve vrste smrznutih proizvoda.',
          label: ''
        },
        productTexts: [
          'Ambalaža za pakovanje ove vrste proizvoda mora da bude izuzetno kvalitetna i izdržljiva na niskim temperaturama. Za zamrznute proizvode naročito je bitna niska propustljivost toplote i svetlosti. HD+ flekso-štampa uz kombinaciju visokokvalitetnih barijernih materijala obezbeđuje odličan odnos cene i kvaliteta.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-sladoledi-i-smrznuti-proizvodi02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja za sve vrste smrznutih proizvoda.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-promocije': {
        productName: 'PROMO PAKOVANJA',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-promo-pakovanja01',
          alt: 'Na slici su prikazana fleksibilna promotivna pakovanja.',
          label: ''
        },
        productTexts: [
          'Tanke termo-varive promotivne trake, štampane u flekso ili rotogravurnoj štampi visokog kvaliteta. Dostupne su na jednostrukom ili dupleks providnom ili neprozirnom materijalu, pogodnom za VFFS linije za pakovanje. Jednostavna, ali pametna aplikacija koja može u potpunosti podržati vaše promotivne akcije.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-promo-pakovanja02',
              alt: 'Na slici su prikazana fleksibilna promotivna pakovanja.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/sr/proizvodni-program/ambalaza-voda-sokovi': {
    categoryData: {
      categoryNameShort: 'Industrija pića',
      categoryName: 'Fleksibilna ambalaža - Industrija pića',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/sr/proizvodniprogram/ambalaza-industrija-pica',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za industriju pića.',
          label: ''
        }
      ],
      categoryTexts: [
        'Proizvodi od fleksibilnih materijala pronašli su široku primenu i u industriji pića kao nezaobilazni dodatak PET pakovanjima',
        'Pored potrebe da se istaknu sve neophodne informacije o samom proizvodu, omotna i zbirna pakovanja imaju specifičnu ulogu i u jačanju samog brenda, a kupci su već navikli da na osnovu privlačne ambalaže lako prepoznaju svoje omiljene napitke.'
      ]
    },
    categoryProducts: {
      'ambalaza-omotna-etiketa': {
        productName: 'OMOTNA ETIKETA',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-omotna-etiketa01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja omotne etikete za plastične flaše.',
          label: ''
        },
        productTexts: [
          'Ovo je omotna fleksibilna ambalaža visoke rezolucije dobijena uz pomoć HD+ flekso ili duboke roto-gravure, atraktivnog izgleda i prestižnog kvaliteta. Zahteva primenu specifične opreme radi smanjenja prisustva statičkog elektriciteta, što obezbeđuje neometan rad prilikom formiranja ove vrste ambalaže.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-omotna-etiketa02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja omotne etikete za plastične flaše.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-stand-up-pouch': {
        productName: 'STAND UP POUCH',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-standup-pouch01',
          alt: 'Na slici su prikazana fleksibilna pakovanja stand up pouch.',
          label: ''
        },
        productTexts: [
          'Stand up fleksibilna ambalaža predstavlja pakovanje modernog dizajna, koje ovim kesama sa umetnutim dnom omogućava da same stoje na policama, dok je ravna površina vreće idealna za različita idejna rešenja primamljivog dizajna. Pogodna je za pakovanje različitih vrsta prehrambenih, neprehrambenih, praškastih, dehidriranih, hemijskih, tečnih i drugih proizvoda. Odlična su zamena za dosada korišćena bag in a box pakovanja, staklena i metalna pakovanja.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-standup-pouch02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja stand up pouch.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/sr/proizvodni-program/ambalaza-kozmetika-farmacija': {
    categoryData: {
      categoryNameShort: 'Kozmetika i suplementi',
      categoryName:
        'Fleksibilna ambalaža - Kozmetička i farmaceutska industrija',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/sr/proizvodniprogram/ambalaza-kozmeticka-i-farmaceutskaindustrija',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za kozmetiku i suplemente.',
          label: ''
        }
      ],
      categoryTexts: [
        'Priozvodi kozmetičke i farmaceutske industrije su naročito zahtevni jer su to najčešće proizvodi u vidu tečnih i praškastih supstanci različitog hemijskog sastava, agresivnih i higroskopnih svojstava.',
        'Ambalaža neophodna za ovu grupu artikala mora da obezbedi adekvatnu otpornost pakovanja na hemijski sastav proizvoda, da zadovolji visoke sanitarne standarde i, naravno, da obezbedi visoku otpornost pakovanja na mehaničke i spoljašnje uticaje.'
      ]
    },
    categoryProducts: {
      'ambalaza-kozmetika-higijena': {
        productName: 'KOZMETIKA I HIGIJENA',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-kozmetika-i-higijena01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja za kozmetiku i higijenu.',
          label: ''
        },
        productTexts: [
          'Ova grupa proizvoda zahteva troslojne ili četvoroslojne barijerne materijale, koji sprečavaju razmenu gasova sadržaja i okruženja, čime se sprečava slabljenje hemijskih primesa samog proizvoda. Ambalažu odlikuje zatezno-kompresiona moć, a pritom, veoma lako i jednostavno se koristi. Moguće je primeniti kako flekso tehnologiju štampe, tako i roto-gravuru za liniju premijum proizvoda.'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-kozmetika-i-higijena02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja za kozmetiku i higijenu.',
              label: ''
            }
          ]
        }
      },
      'ambalaza-farmaceutski-proizvodi': {
        productName: 'FARMACEUTSKI PROIZVODI',
        productImgs: {
          path:
            'assets/img/papirprint/sr/proizvodi/ambalaza-farmaceutski-proizvodi01',
          alt:
            'Na slici su prikazana fleksibilna pakovanja suplementske proizvode.',
          label: ''
        },
        productTexts: [
          'Fleksibilna ambalaža u svetu poznata kao blister paket (šupljina ili džep formirani unutar pakovanja) koja štiti proizvod od spoljnih faktora kao što su vlažnost i kontaminacija na duži vremenski period. Neprozirni blisteri takođe štite osetljive proizvode od uticaja dnevnog svetla. Ova ambalaža obično se koristi kao jedinična doza za pakovanje farmaceutskih tableta, kapsula ili pastila, obezbeđujući zaštitu pri odlaganju proizvoda (police, rafofi).'
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path:
                'assets/img/papirprint/sr/proizvodi/ambalaza-farmaceutski-proizvodi02',
              alt:
                'Na slici su prikazana fleksibilna pakovanja suplementske proizvode.',
              label: ''
            }
          ]
        }
      }
    }
  },
  '/sr/proizvodni-program/proizvodnja-pe-filma': {
    categoryData: {
      categoryNameShort: 'Polietilenski filmovi',
      categoryName: 'Proizvodnja repromaterijala - polietilenski film',
      categoryImgs: [
        {
          path:
            'assets/img/papirprint/sr/proizvodniprogram/polietilenski-filmi',
          alt: 'Na slici su prikazana granulat i polietilenski film.',
          label: ''
        }
      ],
      categoryTexts: [
        'Širok asortiman proizvoda za pakovanje upotpunjen je proizvodnjom polietilenskih filmova za sopstvene potrebe i za potrebe kupaca.  Papir Print proizvodi LDPE filmove za razne namene u opsegu debljina od 25 µm do 120 µm.',
        'Polietilenska ambalaža štiti proizvod i produžava mu vek trajanja, uz mogućnost raznovrsnog oblikovanja i štampe. Ambalaža od polietilena mora da zadovolji  izuzetno stroge zahteve zdravstvene ispravnosti pa se kontrola ambalaže sprovodi kontinualno u relevantnim ovlašćenim institucijama.'
      ]
    },
    categoryProducts: {
      'proizvodnja-polietilen': {
        productName: 'PROIZVODNJA POLIETILENSKOG FILMA',
        productImgs: {
          path: 'assets/img/papirprint/sr/proizvodi/polietilenski-film',
          alt: 'Na slici je prikazan polietilenski duvani balon.',
          label: ''
        },
        productTexts: [
          'Prateći trendove i tržište, značajno unapređenje tehničkih i tehnoloških postupaka u proizvodnji postignuto je uvođenjem linije za koekstruziju duvanog polietilena. ',
          'Od 2014. godine naša kompanija poseduje troslojni koekstruder poznatog brenda Windmoller&Holcher. Osnovne karakteristike ogledaju se u mogućnostima izrade polietilena debljine u rasponu od 20 do 200 µm, kao i formata od 800 do 1800 mm i maksimalnim kapacitetom od 870 kg/h. ',
          'Umnožavanje sveukupnog znanja tima omogućava da PAPIR PRINT radi kako za potrebe svoje proizvodnje, tako i da odgovori zahtevima drugih kupaca. U saradnji sa laboratorijom potvrđuje se kvalitet proizvodnje polietilena. '
        ],
        productExtras: {
          'Vizuelni izgled i vrsta štampe': {
            standard: []
          },
          'Strukture materijala i barijerne osobine': {
            standard: []
          },
          'Za linuju premium proizvoda': {},
          imgs: [
            {
              path: 'assets/img/papirprint/sr/proizvodi/polietilenski-film',
              alt: 'Na slici je prikazan polietilenski duvani balon.',
              label: ''
            }
          ]
        }
      }
    }
  }
};
