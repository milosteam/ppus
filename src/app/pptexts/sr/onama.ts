export const ONAMASR = [
  {
    title: 'O Kompaniji',
    description:
      'Sa pravom možemo reći da imamao najkvalitetniju flekso-štampu kojom možemo da odgovorimo svim zahtevima naših kupaca. ' +
      'Mi proizvodimo ambalažu sutrašnjice DANAS. Dobro došli u PAPIR PRINT!'
  },
  {
    title: 'Profil kompanije',
    texts: [
      'PAPIR PRINT DOO je društvo sa ograničenom odgovornošću, koje postoji i uspešno posluje na domaćem, ali i na inostranom tržištu već 26 godina. ' +
        'Osnovna delatnost je grafička delatnost – štampanje. Kompanija od nastanka pa do danas beleži kontinuirani rast poslovnih kapaciteta, što se direktno ogleda u povećanju obima proizvodnje iz godine u godinu. ' +
        'Proizvodni asortiman preduzeća namenjen je pretežno prehrambenoj, hemijskoj i farmaceutskoj industriji, a fleksibilnost i kooperativan odnos omogućava ' +
        'proizvodnju ambalaže i za sve druge industrijske grane po specifikaciji kupca. ',
      'PAPIR PRINT ispunjava visoke kriterijume, prema zahtevima ISO: 9001 : 2015, ISO: 14001 : 2015 i ISO 22000 ( HACCP), kao i zahteve savremenog poslovanja za ' +
        'kratkim rokovima isporuke proizvoda visokog kvaliteta i bezbednih za pakovanje artikala koji su najosetljiviji.',
      'Kompanija prati i implementira sva tehnološka dostignuća u oblasti flekso-štampe. Rezultat savremene proizvodnje i dugogodišnjeg iskustva ' +
        'potvrđen je osvajanjem više nagrada, od kojih su najznačajnije nagrade osvojene u Londonu za ostvarenja u flekso-štampi i nagrada za najbolji ' +
        'otisak u kategriji narrow web pod pokroviteljstvom organizacije Flexo Tech International.'
    ],
    img: {
      path: 'assets/img/papirprint/sr/kompanija/kompanija-papirprint',
      alt: '',
      label: ''
    }
  },
  {
    title: 'Misija i vizija',
    texts: [
      'NAŠA MISIJA - Mi smo rezultatski orijentisana štamparska firma, posvećena razvoju odnosa sa svojim klijentima, pružajući ekspertizu za razvoj i unapređenje vrednosti njihovih proizvoda i pri tom osiguravajući profitabilnost kako klijenata, tako i naše kompanije. PAPIR PRINT, kao organizacija posvećena vrednostima, nudi najviši kvalitet štampe kroz iskustvo naših ljudi i vrhunsku tehnologiju. Mi održavamo saradnju sa svojim klijentima primenom etičkih normi u socijalno odgovornom radnom okruženju.',
      'NAŠA VIZIJA - Naše vrednosti zasnovane na principima biće baza za naš budući razvoj. Stimulativno radno mesto zajedno sa razvijenim sistemima komunikacije, treninzi, razmena znanja, podsticanje individualnog rasta do punog potencijala osiguraće nam potpuno zadovoljstvo kako klijenata tako i radnika. PAPIR PRINT  teži da postane sinonim za najkvalitetniju štampu, a pre svega sinonim za kvalitet u izradi najsavremenije fleksibilne ambalaže, sa maksimalnom efikasnošću i efektivnošću, uz primenu međunarodnih standarda, kako u Srbiji, tako i u svim zemljama istočne i zapadne Evrope.'
    ],
    img: {
      path: 'assets/img/papirprint/sr/kompanija/misija-vizija',
      alt: '',
      label: ''
    }
  },

  {
    title: 'DRUŠTVENA ODGOVORNOST',
    texts: [
      'ODGOVORNOST* Mi u PAPIR PRINTU držimo svoja obećanja i ispunjavamo obaveze prema drugima i lično smo posvećeni uspehu i dobrobiti tima. ',
      'LOJALNOST* Postavili smo lojalnost u srce našeg poslovanja. ',
      'KORPORATIVNA DRUŠTVENA ODGOVORNOST* Mi u PAPIR PRINTU obavljamo svoj posao u skladu sa najvišim međunarodnim etičkim standardima, poštujući ljude i okolinu. Dugi niz godina deo smo globalne SEDEX platforme. ',
      'PARTNERSTVO* Mi visoko vrednujemo i poštujemo zaposlene, ljude koji predstavljaju stubove naše kompanije i njihovu posvećenost poslu. Regrutovanje, obuka i čuvanje kvalitetnih ljudi jedan je od osnovnih ciljeva PAPIR PRINTA. ',
      'INOVACIJE* Inovacije na svim poljima su od ključnog značaja za razvoj našeg poslovanja. One su prioritet naše organizacije u razvoju novih proizvoda i procesa kojima se unapređuju proizvodni kapaciteti. ',
      'ENERGIJA I EKOLOGIJA* Naš stalni cilj je minimizacija otpada i recilklaža, iskorišćavanje sekundarnih sirovina i optimizacija energetske efikasnosti. Ova izjava čvrsto je podržana sertifikovanim  ISO 14001 standardom, a u toku je sprovođenje standarda ISO 50001. '
    ],
    img: {
      path: 'assets/img/papirprint/sr/kompanija/drustvena-odgovornost',
      alt: '',
      label: ''
    }
  },

  {
    title: 'POLITIKA INTEGRISANIH SISTEMA MENADŽMENTA',
    texts: [
      'Politika integrisanih sistema menadžmenta se ogleda u uspostavljanju, održavanju i unapređenju integrisanih sistema menadžmenta u skladu sa zahtevima međunarodnih standarda ISO 9001 : 2015, ISO 14001 : 2015 i ISO 22000, čijom primenom se obezbeđuje sklad između organizacije i interesa svih relevantnih zainteresovanih strana kao i ostvarenje vizije, misije i strateških usmerenja oragnizacije. PAPIR PRINT d.o.o. proizvodi fleksibilnu ambalažu  za prehrambenu, farmaceutsku i hemijsku industriju. Proizvodnja fleksibilne ambalaže treba da bude tako organizovana da ispuni sve zakonske obaveze vezano za kvalitet proizvoda, životnu sredinu kao i bezbednost proizvoda za pakovanje hrane, da obezbedi kvalitet koji će ispuniti zahteve kupca uz smanjenje troškova i povećanje produktivnosti. ',
      'Rukovodstvo i zaposleni u PAPIR PRINTU su odgovorni za sprovođenje politike IMS-a, a u skladu sa standardima SRPS ISO 9001 : 2015, SRPS ISO 14001 : 2015 i SRPS ISO 22000.'
    ],
    img: {
      path:
        'assets/img/papirprint/sr/kompanija/politikaintegrisanih-sistema-menadzmenta',
      alt: '',
      label: ''
    }
  }
];
