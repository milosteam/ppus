export const CONTACTSR = {
  header: {
    title: 'Kontakt info',
    text:
      'Hvala Vam što ste posetili naš Web Portal i što ste zainteresovani za naše usluge. Možete kontaktirati sa nama putem veb-sajta, imejla, telefonom ili nas lično posetiti. Sve kontaktne informacije su dostupne na ovoj stranici.'
  },
  data: [{}, {}]
};
